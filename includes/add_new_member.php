<?php
include_once 'db_conn.php';
include_once 'functions.php';

sec_session_start(); // Our custom secure way of starting a PHP session.

if (isset($_POST['member_fname'], $_POST['member_type'], $_POST['member_lname'], $_POST['member_date'], $_POST['member_school'])) {
    $member_fname = filter_input(INPUT_POST, 'member_fname', FILTER_SANITIZE_STRING);
    $member_lname = filter_input(INPUT_POST, 'member_lname', FILTER_SANITIZE_STRING);
    $member_type = filter_input(INPUT_POST, 'member_type', FILTER_SANITIZE_STRING);
    $member_date = date_create_from_format('m/d/Y', $_POST['member_date'])->format('Y-m-d');
    //$member_date = date("Y-m-d", strtotime($_POST['member_date']));
    //$member_date = $_POST['member_date'];
    $school_id = $_POST['member_school'];

    // Insert the new user into the database
    if ($insert_stmt = $conn->prepare("INSERT INTO `Members` (`MemberFName`, `MemberLName`, `Type`, `EntryDate`, `SchoolID`) VALUES (?, ?, ?, ?, ?)")) {
        $insert_stmt->bind_param('sssss', $member_fname, $member_lname, $member_type, $member_date, $school_id);
        // Execute the prepared query.
        if (! $insert_stmt->execute()) {
            header('Location: ../new_member.php?err=FAILURE');
        }
    }
    header('Location: ../new_member.php?err=SUCCESS');
}