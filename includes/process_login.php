<?php
include_once 'db_conn.php';
include_once 'functions.php';

sec_session_start(); // Our custom secure way of starting a PHP session.

if (isset($_POST['username'], $_POST['p'])) {
    $user_name = $_POST['username'];
    $password = $_POST['p']; // The hashed password.

    if (login($user_name, $password, $conn) == 0) {
        // Login success
        header('Location: ../dashboard.php');
    }
    elseif (login($user_name, $password, $conn) == 2){
        header('Location: ../index.php?error=2');
    }
    else {
        // Login failed
        header('Location: ../index.php?error=1');
        //echo $user_name;
        //echo $password;
    }
} else {
    // The correct POST variables were not sent to this page.
    echo 'Invalid Request';
}