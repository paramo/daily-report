<?php
include_once 'config.php';
include_once 'password.php';

function sec_session_start() {
    $session_name = 'sec_session_id';   // Set a custom session name
    $secure = false;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"],
        $cookieParams["domain"],
        $secure,
        $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session
    session_regenerate_id(true);    // regenerated the session, delete the old one.
}

function login($user_name, $password, $mysqli) {
    $error = 0;
    // Using prepared statements means that SQL injection is not possible.
    if ($stmt = $mysqli->prepare("SELECT user_id, username, password, role FROM Users WHERE username = ? LIMIT 1")) {
        $stmt->bind_param('s', $user_name);  // Bind "$user_name" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
        // get variables from result.
        $stmt->bind_result($user_id, $username, $db_password, $user_role);
        $stmt->fetch();

        if ($stmt->num_rows == 1) {
            // If the user exists we check if the account is locked
            // from too many login attempts

            if (checkbrute($user_id, $mysqli) == true) {
                // Account is locked
                // Send an email to user saying their account is locked
                $error = 2;
            } else {
                // Check if the password in the database matches
                // the password the user submitted. We are using
                // the password_verify function to avoid timing attacks.
                if (password_verify($password, $db_password)) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                $user_browser = $_SERVER['HTTP_USER_AGENT'];
                // XSS protection as we might print this value
                $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                $_SESSION['user_id'] = $user_id;
                // XSS protection as we might print this value
                $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);
                $_SESSION['username'] = $username;
                $_SESSION['user_role'] = $user_role;
                $_SESSION['login_string'] = hash('sha512', $db_password . $user_browser);
                // Login successful.
                    $error = 0;
            } else {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts(user_id, time) VALUES ('$user_id', '$now')");
                    $error = 1;
                }
            }
        } else {
            // No user exists.
            $error = 4;
        }
    }
    return $error;
}

function checkbrute($user_id, $mysqli) {
    // Get timestamp of current time
    $now = time();

    // All login attempts are counted from the past 2 hours.
    $valid_attempts = $now - (2 * 60 * 60);

    if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);

        // Execute the prepared query.
        $stmt->execute();
        $stmt->store_result();

        // If there have been more than 5 failed logins
        if ($stmt->num_rows > 5) {
            return true;
        } else {
            return false;
        }
    }
}

function login_check($mysqli) {
    // Check if all session variables are set
    if (isset($_SESSION['user_id'],
    $_SESSION['username'],
    $_SESSION['login_string'])) {

        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];

        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];

        if ($stmt = $mysqli->prepare("SELECT password FROM Users WHERE user_id = ? LIMIT 1")) {
            // Bind "$user_id" to parameter.
            $stmt->bind_param('i', $user_id);
            $stmt->execute();   // Execute the prepared query.
            $stmt->store_result();

            if ($stmt->num_rows == 1) {
                // If the user exists get variables from result.
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);

                if (hash_equals($login_check, $login_string)) {
                    // Logged In!!!!
                    return true;
            } else {
                    // Not logged in
                    return false;
                }
            } else {
                // Not logged in
                return false;
            }
        } else {
            // Not logged in
            return false;
        }
    } else {
        // Not logged in
        return false;
    }
}

function esc_url($url) {

    if ('' == $url) {
        return $url;
    }

    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);

    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;

    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }

    $url = str_replace(';//', '://', $url);

    $url = htmlentities($url);

    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);

    if ($url[0] !== '/') {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}

function newPassword($mysqli){
    $error_msg = "";
    if (isset($_POST['user'], $_POST['p'])) {
        // Sanitize and validate the data passed in
        $userID = $_POST['user'];

        $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
        if (strlen($password) != 128) {
            // The hashed pwd should be 128 characters long.
            // If it's not, something really odd has happened
            $error_msg .= '<p class="error">Invalid password configuration.</p>';
        }

        if (empty($error_msg)) {

            // Create hashed password using the password_hash function.
            // This function salts it with a random salt and can be verified with
            // the password_verify function.
            $password = password_hash($password, PASSWORD_BCRYPT);

            // Insert the new user into the database
            $query = "UPDATE Users SET password ='".$password."' WHERE user_id = '". $userID."'";
            if ($mysqli->query($query) === TRUE) {
                header('Location: ./password_recovery.php?err=Success');
            } else {header('Location: ./password_recovery.php?err=Failure');}

        }
    }
}
// UPDATE USER DATA
function updateUserData($mysqli){
    $error_msg = "";
    if (isset($_POST['user_id'], $_POST['username'], $_POST['email'], $_POST['member_school'], $_POST['p'])) {
        // Sanitize and validate the data passed in
        $userID = $_POST['user_id'];
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $school = $_POST['member_school'];

        $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
        if (strlen($password) != 128) {
            // The hashed pwd should be 128 characters long.
            // If it's not, something really odd has happened
            $error_msg .= '<p class="error">Invalid password configuration.</p>';
        }

        if (empty($error_msg)) {

            // Create hashed password using the password_hash function.
            // This function salts it with a random salt and can be verified with
            // the password_verify function.
            $password = password_hash($password, PASSWORD_BCRYPT);

            // Insert the new user into the database
            $query = "UPDATE Users SET username = '".$username."', school = '".$school."', email = '".$email."', password ='".$password."' WHERE user_id = '". $userID."'";
            if ($mysqli->query($query) === TRUE) {
                //header('Location: ../adm/user.php?usrid='.$userID.'?err=SUCCESS');
                return "User information successfully Updated.";
            } else {
            //header('Location: ../adm/user.php?usrid='.$userID.'?err=FAILURE');
                return "ERROR: User information could not be Updated.";
            }

        }
    } else {return "";}
}
// UPDATE MEMBER DATA
function updateMemberData($mysqli){
    if (isset($_POST['user_id'], $_POST['member_fname'], $_POST['member_lname'], $_POST['member_type'], $_POST['member_date'])) {
        // Sanitize and validate the data passed in
        $userID = $_POST['user_id'];
        $memberfname = filter_input(INPUT_POST, 'member_fname', FILTER_SANITIZE_STRING);
        $memberlname = filter_input(INPUT_POST, 'member_lname', FILTER_SANITIZE_STRING);
        $memberType = filter_input(INPUT_POST, 'member_type', FILTER_SANITIZE_STRING);
        $memberdate = date_create_from_format('m/d/Y', $_POST['member_date'])->format('Y-m-d');


            $query = "UPDATE Members SET MemberFName = '".$memberfname."', MemberLName = '".$memberlname."', Members.Type = '".$memberType."', EntryDate = '".$memberdate."' WHERE `MemberID` = ". $userID;
            if ($mysqli->query($query) === TRUE) {
                //header('Location: ../adm/user.php?usrid='.$userID.'?err=SUCCESS');
                return "Member information successfully Updated.";
            } else {
                //header('Location: ../adm/user.php?usrid='.$userID.'?err=FAILURE');
                return "ERROR: Member information could not be Updated.";
            }
    } else {return "";}
}

function enterNewSchool($mysqli) {
    if (isset($_POST['schoolname'])) {
        // Sanitize and validate the data passed in
        $schoolname = filter_input(INPUT_POST, 'schoolname', FILTER_SANITIZE_STRING);

            // Insert the new school into the database
            $query = "INSERT INTO School(Name) VALUES ('$schoolname')";
            if ($mysqli->query($query) === TRUE) {
                return "New School added to the database.";
            } else {
                return "ERROR: Unable to add new School.";
            }
    } else {return "";}
}

function menu_sidebar(){
     echo '<div class="menu-bar">
            <ul>
               <li>
                  <a href="dashboard.php">
                     <i class="glyphicon glyphicon-th-list"></i> <span>Members List</span>
                  </a>
               </li>
               <li>
                  <a href="new_member.php" >
                     <i class="glyphicon glyphicon-user"></i> <span>New Member</span>
                  </a>
               </li>
               <!--<li>
                  <a href="merchandise.php">
                     <i class="glyphicon glyphicon-tag"></i> <span>Merchandise</span>
                  </a>
               </li>-->
               <li>
                  <a href="billing.php">
                     <i class="glyphicon glyphicon-usd"></i> <span>Billing</span>
                  </a>
               </li>
               <li>
                  <a href="appointments.php">
                     <i class="glyphicon glyphicon-comment"></i> <span>Appointments</span>
                  </a>
               </li>
            </ul>
         </div>';
}
function menu_sidebar_admin(){
    echo '<div class="menu-bar">
            <ul>
                <li>
                    <a href="dashboard_admin.php" >
                        <i class="glyphicon glyphicon-check"></i> <span>Master Report</span>
                    </a>
                </li>
                <li>
                    <a href="members_report.php">
                        <i class="glyphicon glyphicon-user"></i> <span>Member Tracker</span>
                    </a>
                </li>
                <li>
                    <a href="chart.php">
                        <i class="glyphicon glyphicon-stats"></i> <span>Chart</span>
                    </a>
                </li>
                <hr/>
				<li>
                  <a href="merchandise.php">
                     <i class="glyphicon glyphicon-tag"></i> <span>Merchandise</span>
                  </a>
               </li>
			    <hr/>
                <li>
                    <a href="users.php">
                        <i class="glyphicon glyphicon-user"></i> <span>Users</span>
                    </a>
                </li>
                <li>
                    <a href="schools.php">
                        <i class="glyphicon glyphicon-bookmark"></i> <span>Schools</span>
                    </a>
                </li>
                <li>
                    <a href="expenses.php">
                        <i class="glyphicon glyphicon-usd"></i> <span>Expenses</span>
                    </a>
                </li>
                <li>
                    <a href="company.php">
                        <i class="glyphicon glyphicon-book"></i> <span>Company</span>
                    </a>
                </li>
            </ul>
        </div>';
}

//SELECT SCHOOLS
function listSchools($mysqli) {
    $usrid = $_SESSION['user_id'];
    $checkQuery = "SELECT `role`, `school` FROM `Users` WHERE `user_id` = ".$usrid;
    $userData = $mysqli->query($checkQuery);
    $userRow = $userData->fetch_assoc();
    $userRole = $userRow['role'];
    $userSchool = $userRow['school'];

    if($userRole == "administrator") {
    $query = "SELECT `SchoolID`, `Name` FROM `School`";
    } else {
    $query = "SELECT `SchoolID`, `Name` FROM `School` WHERE `SchoolID` = ".$userSchool;
    }
    $schools = $mysqli->query($query);

    $schools_select = "<select name='member_school' class='school_select'>";
    while (($row = $schools->fetch_array()) != null)
    {
        $schools_select .= "<option value = '{$row['SchoolID']}'";
        $schools_select .= ">{$row['Name']}</option>";
    }
    $schools_select .= "</select>";
    return $schools_select;
}

//TABLE SCHOOLS
function tableSchools($mysqli) {
    $query = "SELECT `SchoolID`, `Name` FROM `School`";
    $schools = $mysqli->query($query);

    $schools_table = "";
    while (($row = $schools->fetch_array()) != null)
    {
        $schools_table .= '<tr class="rpt-member"><td>'.$row['SchoolID'].'</td><td>'.$row['Name'].'</td></tr>';
    }
    return $schools_table;
}

//LISTAR INSTRUCTORES PANEL ADMIN
function admListInstructor($mysqli){
    $query = "SELECT `user_id`, `username`, `email`, `School`.`Name` AS `schoolname` FROM `Users` INNER JOIN `School` ON `SchoolID` = `Users`.`school`";
    $users = $mysqli->query($query);

    $users_list = '';
    while (($row = $users->fetch_array()) != null)
    {
        $user_id = $row['user_id'];
        $username = $row['username'];
        $user_email = $row['email'];
        $schoolname = $row['schoolname'];

        $users_list .= '<tr class="rpt-member">';
        $users_list .= '<td><label><a href="user.php?usrid='.$user_id.'">'.$username.'</a></label></td>';
        $users_list .= '<td><label>'.$user_email.'</label></td>';
        $users_list .= '<td><label>'.$schoolname.'</label></td>';
        $users_list .= '</tr>';
    }
    return $users_list;
}

//LISTAR MEMBERS
function listMembers($mysqli){

    $per_page=20;
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page=1;
    }
    $start_from = ($page-1) * $per_page;

    $usrid = $_SESSION['user_id'];
    $checkQuery = "SELECT `role`, `school` FROM `Users` WHERE `user_id` = ".$usrid;
    $userData = $mysqli->query($checkQuery);
    $userRow = $userData->fetch_assoc();
    $userRole = $userRow['role'];
    $userSchool = $userRow['school'];

    if($userRole == "administrator") {
        $query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `Members`.`Type`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = 0 ORDER BY `MemberLName` LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = 0";
    } else {
        $query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `Members`.`Type`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = 0 AND `Members`.`SchoolID` = ".$userSchool." ORDER BY `MemberLName` LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = 0 AND `Members`.`SchoolID` = ".$userSchool;
    }

    //$query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `SchoolID` = `Members`.`SchoolID`";
    $users = $mysqli->query($query);

    $member_list = '<table class="table" id="simpleTable"><thead><tr style="background-color: #313131; color: #fff ">';
    $member_list .= '<th data-sort="string">Member <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">Type <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">School <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">Entry Date <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '</tr></thead><tbody>';

    while (($row = $users->fetch_array()) != null)
    {
        $memberID = $row['MemberID'];
        $MemberFName = $row['MemberFName'];
        $MemberLName = $row['MemberLName'];
        $MemberType = $row['Type'];
        $EntryDate = date_create_from_format('Y-m-d', $row['EntryDate'])->format('m/d/Y');
        $schoolname = $row['schoolname'];

        $member_list .= '<tr class="rpt-member">';
        $member_list .= '<td><label><a href="details.php?usrid='.$memberID.'">'.$MemberFName.' '.$MemberLName.'</a></label></td>';
        $member_list .= '<td><label>'.$MemberType.'</label></td>';
        $member_list .= '<td><label>'.$schoolname.'</label></td>';
        $member_list .= '<td><label>'.$EntryDate.'</label></td>';
        $member_list .= '</tr>';
    }
    $member_list .= '</tbody></table>';

    //PAGINATION

    $totalData = $mysqli->query($totalsQuery);
    $totalRows = $totalData->fetch_assoc();
    $total_pages = ceil($totalRows["row_num"] / $per_page);

    $pagination = '<div class="col-sm-12 col-xs-12 col-md-12 pagination-container"><nav><ul class="pagination">';
    $pagination .= '<li><a href="dashboard.php?page=1" aria-label="Previous"><span aria-hidden="true">First</span></a></li>';

    for ($i=1; $i<=$total_pages; $i++) {
        $pagination .= '<li><a href="dashboard.php?page='.$i.'">'.$i.'</a></li>';
    };

    $pagination .= '<li><a href="dashboard.php?page='.$total_pages.'" aria-label="Previous"><span aria-hidden="true">Last</span></a></li>';
    $pagination .= '</ul></nav></div>';

    return $member_list.$pagination;
}
//LISTAR MEMBERS Inactivos
function listInactiveMembers($mysqli){

    $per_page=20;
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page=1;
    }
    $start_from = ($page-1) * $per_page;

    $usrid = $_SESSION['user_id'];
    $checkQuery = "SELECT `role`, `school` FROM `Users` WHERE `user_id` = ".$usrid;
    $userData = $mysqli->query($checkQuery);
    $userRow = $userData->fetch_assoc();
    $userRole = $userRow['role'];
    $userSchool = $userRow['school'];

    if($userRole == "administrator") {
        $query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `Members`.`Type`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = 1 ORDER BY `MemberLName` LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = 1";
    } else {
        $query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `Members`.`Type`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = 1 AND `Members`.`SchoolID` = ".$userSchool." ORDER BY `MemberLName` LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = 1 AND `Members`.`SchoolID` = ".$userSchool;
    }

    //$query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `SchoolID` = `Members`.`SchoolID`";
    $users = $mysqli->query($query);

    $member_list = '<table class="table" id="mercTable"><thead><tr style="background-color: #313131; color: #fff ">';
    $member_list .= '<th data-sort="string">Member <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">Type <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">School <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">Entry Date <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">Make Active</th>';
    $member_list .= '</tr></thead><tbody>';

    while (($row = $users->fetch_array()) != null)
    {
        $memberID = $row['MemberID'];
        $MemberFName = $row['MemberFName'];
        $MemberLName = $row['MemberLName'];
        $MemberType = $row['Type'];
        $EntryDate = date_create_from_format('Y-m-d', $row['EntryDate'])->format('m/d/Y');
        $schoolname = $row['schoolname'];

        $member_list .= '<tr class="rpt-member inactive-member">';
        $member_list .= '<td><label><a href="details.php?usrid='.$memberID.'">'.$MemberFName.' '.$MemberLName.'</a></label></td>';
        $member_list .= '<td><label>'.$MemberType.'</label></td>';
        $member_list .= '<td><label>'.$schoolname.'</label></td>';
        $member_list .= '<td><label>'.$EntryDate.'</label></td>';
        $member_list .= '<td><label><a href="inactive_members.php?activate='.$row['MemberID'].'" class="boton-head ami btn-user-update">Make Active</a></label></td>';
        $member_list .= '</tr>';
    }
    $member_list .= '</tbody></table>';

    //PAGINATION

    $totalData = $mysqli->query($totalsQuery);
    $totalRows = $totalData->fetch_assoc();
    $total_pages = ceil($totalRows["row_num"] / $per_page);

    $pagination = '<div class="col-sm-12 col-xs-12 col-md-12 pagination-container"><nav><ul class="pagination">';
    $pagination .= '<li><a href="dashboard.php?page=1" aria-label="Previous"><span aria-hidden="true">First</span></a></li>';

    for ($i=1; $i<=$total_pages; $i++) {
        $pagination .= '<li><a href="dashboard.php?page='.$i.'">'.$i.'</a></li>';
    };

    $pagination .= '<li><a href="dashboard.php?page='.$total_pages.'" aria-label="Previous"><span aria-hidden="true">Last</span></a></li>';
    $pagination .= '</ul></nav></div>';

    return $member_list.$pagination;
}
//AGREGAR TRANSACCION
function addPayment($mysqli){
    if (isset($_POST["user_id"], $_POST['trnReason'],$_POST['trnDesc'],$_POST['trnDate'],$_POST['trnAmount'],$_POST['trnCollectedBy'],$_POST['trnMethod'])) {
        // Sanitize and validate the data passed in
        $memberID = $_POST["user_id"];
        $instructorID = $_SESSION['user_id'];
        $trnReason = filter_input(INPUT_POST, 'trnReason', FILTER_SANITIZE_STRING);
        $trnDesc = filter_input(INPUT_POST, 'trnDesc', FILTER_SANITIZE_STRING);
        $trnMethod = filter_input(INPUT_POST, 'trnMethod', FILTER_SANITIZE_STRING);
        $trnDate = date_create_from_format('m/d/Y', $_POST['trnDate'])->format('Y-m-d');
        $trnAmount = filter_input(INPUT_POST, 'trnAmount', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		$trnCollectedBy = filter_input(INPUT_POST, 'trnCollectedBy', FILTER_SANITIZE_STRING);
		
        // Insert the new school into the database
        $query = "INSERT INTO Transaction(Reason, Description, EntryDate, PaymentType, Paid, CollectedBy, MemberID, InstructorID) VALUES ('$trnReason', '$trnDesc', '$trnDate', '$trnMethod', $trnAmount, '$trnCollectedBy', $memberID, $instructorID)";
        if ($mysqli->query($query) === TRUE) {
			return "New Transaction added to the database.";
        } else {
			//echo mysqli_errno($mysqli);
            return "ERROR: Unable to add new Transaction.";
        }
    } else {return "";}
}

//LISTAR TRANSACCIONES
function listTransactions($mysqli, $usrid){
    $per_page=20;
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page=1;
    }
    $start_from = ($page-1) * $per_page;

    $query = "SELECT `Reason`, `Description`, `EntryDate`, `PaymentType`, `Paid`, `CollectedBy` FROM `Transaction` WHERE `MemberID` = ".$usrid." ORDER BY `EntryDate` DESC LIMIT $start_from, $per_page";
    $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Transaction` WHERE `MemberID` = ".$usrid;
    $users = $mysqli->query($query);

    $transac_list = '<table class="table" id="payTable">
            <thead>
               <tr style="background-color: #313131; color: #fff ">
                  <th data-sort="string">
                     Reason <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                  </th>
				  <th data-sort="string">
                     Description <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                  </th>
                  <th data-sort="date">
                     Date <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                  </th>
                  <th data-sort="string">
                     Method <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                  </th>
                  <th data-sort="int">
                     Net Collected <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                  </th>
				  <th data-sort="string">
                     Collected by <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                  </th>
               </tr>
            </thead>
            <tbody>';
    while (($row = $users->fetch_array()) != null)
    {
        $trnReason = $row['Reason'];
		$trnDescription = $row['Description'];
        $trnEntryDate = $row['EntryDate'];
        $trnPaymentType = $row['PaymentType'];
        $trnAmount = $row['Paid'];
		$trnCollectedBy = $row['CollectedBy'];

        $transac_list .= '<tr class="rpt-member">';
        $transac_list .= '<td><label>'.$trnReason.'</label></td>';
		$transac_list .= '<td><label>'.$trnDescription.'</label></td>';
        $transac_list .= '<td><label>'.$trnEntryDate.'</label></td>';
        $transac_list .= '<td><label>'.$trnPaymentType.'</label></td>';
        $transac_list .= '<td><label>'.$trnAmount.'</label></td>';
		$transac_list .= '<td><label>'.$trnCollectedBy.'</label></td>';
        $transac_list .= '</tr>';
    }
    $transac_list .= '</tbody></table>';

    //PAGINATION

    $totalData = $mysqli->query($totalsQuery);
    $totalRows = $totalData->fetch_assoc();
    $total_pages = ceil($totalRows["row_num"] / $per_page);

    $pagination = '<div class="col-sm-12 col-xs-12 col-md-12 pagination-container"><nav><ul class="pagination">';
    $pagination .= '<li><a href="details.php?usrid='.$_GET["usrid"].'&page=1" aria-label="Previous"><span aria-hidden="true">First</span></a></li>';

    for ($i=1; $i<=$total_pages; $i++) {
        $pagination .= '<li><a href="details.php?usrid='.$_GET["usrid"].'&page='.$i.'">'.$i.'</a></li>';
    };

    $pagination .= '<li><a href="details.php?usrid='.$_GET["usrid"].'&page='.$total_pages.'" aria-label="Previous"><span aria-hidden="true">Last</span></a></li>';
    $pagination .= '</ul></nav></div>';

    return $transac_list.$pagination;
}

//AGREGAR MERCHANDISE
function addMerchandise($mysqli){
    if (isset($_POST["merCust"], $_POST['merchandise'],$_POST['merDesc'],$_POST['merDate'],$_POST['merMethod'],$_POST['merAmount'])) {
        // Sanitize and validate the data passed in
        $instructorID = $_SESSION['user_id'];
        $merCust = filter_input(INPUT_POST, 'merCust', FILTER_SANITIZE_STRING);
        $merchandise = filter_input(INPUT_POST, 'merchandise', FILTER_SANITIZE_STRING);
        $merDesc = filter_input(INPUT_POST, 'merDesc', FILTER_SANITIZE_STRING);
        $merDate = date_create_from_format('m/d/Y', $_POST['merDate'])->format('Y-m-d');
        $merMethod = filter_input(INPUT_POST, 'merMethod', FILTER_SANITIZE_STRING);
        $merAmount = filter_input(INPUT_POST, 'merAmount', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        // Insert the new school into the database
        $query = "INSERT INTO Merchandise(Customer, Merchandise, Description, PaymentType, Merchandise.Date, NetCollected, InstructorID) VALUES ('$merCust', '$merchandise', '$merDesc', '$merMethod', '$merDate', $merAmount, $instructorID)";
        if ($mysqli->query($query) === TRUE) {
            return "Merchandise Transaction added to the database.";
        } else {
            return "ERROR: Unable to add Merchandise Transaction.";
        }
    } else {return "";}
}

//LISTAR MERCHANDISE
function listMerchandise($mysqli){
    $per_page=20;
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page=1;
    }
    $start_from = ($page-1) * $per_page;

    $usrid = $_SESSION['user_id'];
    $checkQuery = "SELECT `role` FROM `Users` WHERE `user_id` = ".$usrid;
    $userData = $mysqli->query($checkQuery);
    $userRow = $userData->fetch_assoc();
    $userRole = $userRow['role'];

    $query = "";
    if($userRole == "administrator") {
        $query = "SELECT `MerchandiseID`, `Customer`, `Merchandise`, `Date`, `PaymentType`, `NetCollected` FROM `Merchandise` ORDER BY `Date` DESC LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Merchandise`";
    } else {
        $query = "SELECT `MerchandiseID`, `Customer`, `Merchandise`, `Date`, `PaymentType`, `NetCollected` FROM `Merchandise` WHERE `InstructorID` = ".$usrid." ORDER BY `Date` DESC LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Merchandise` WHERE `InstructorID` = ".$usrid;
    }
    $users = $mysqli->query($query);

    $merc_list = '<table class="table" id="mercTable">
                <thead>
                    <tr style="background-color: #313131; color: #fff ">
                        <th data-sort="string">
                            Customer <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Merchandise <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="date">
                            Date <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Method <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="int">
                            Net Collected <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>';
    while (($row = $users->fetch_array()) != null)
    {
        $mercID = $row['MerchandiseID'];
        $mercCustomer = $row['Customer'];
        $merchandise = $row['Merchandise'];
        $mercDate = date_create_from_format('Y-m-d', $row['Date'])->format('m/d/Y');
        $mercMethod = $row['PaymentType'];
        $mercCollected = $row['NetCollected'];

        $merc_list .= '<tr class="rpt-member">';
        $merc_list .= '<td><label><a href="merchandise_detail.php?merid='.$mercID.'">'.$mercCustomer.'</a></label></td>';
        $merc_list .= '<td><label>'.$merchandise.'</label></td>';
        $merc_list .= '<td><label>'.$mercDate.'</label></td>';
        $merc_list .= '<td><label>'.$mercMethod.'</label></td>';
        $merc_list .= '<td><label>'.$mercCollected.'</label></td>';
        $merc_list .= '</tr>';
    }
    $merc_list .= '</tbody></table>';

    //PAGINATION

    $totalData = $mysqli->query($totalsQuery);
    $totalRows = $totalData->fetch_assoc();
    $total_pages = ceil($totalRows["row_num"] / $per_page);

    $pagination = '<div class="col-sm-12 col-xs-12 col-md-12 pagination-container"><nav><ul class="pagination">';
    $pagination .= '<li><a href="merchandise.php?page=1" aria-label="Previous"><span aria-hidden="true">First</span></a></li>';

    for ($i=1; $i<=$total_pages; $i++) {
        $pagination .= '<li><a href="merchandise.php?page='.$i.'">'.$i.'</a></li>';
    };

    $pagination .= '<li><a href="merchandise.php?page='.$total_pages.'" aria-label="Previous"><span aria-hidden="true">Last</span></a></li>';
    $pagination .= '</ul></nav></div>';

    return $merc_list.$pagination;
}

// UPDATE MERCHANDISE DATA
function updateMercData($mysqli){
    if (isset($_POST["merc_id"],$_POST["merCust"], $_POST['merchandise'],$_POST['merDesc'],$_POST['merDate'],$_POST['merMethod'],$_POST['merAmount'])) {
        // Sanitize and validate the data passed in
        $mercID = $_POST['merc_id'];
        $merCust = filter_input(INPUT_POST, 'merCust', FILTER_SANITIZE_STRING);
        $merchandise = filter_input(INPUT_POST, 'merchandise', FILTER_SANITIZE_STRING);
        $merDesc = filter_input(INPUT_POST, 'merDesc', FILTER_SANITIZE_STRING);
        $merDate = date_create_from_format('m/d/Y', $_POST['merDate'])->format('Y-m-d');
        $merMethod = filter_input(INPUT_POST, 'merMethod', FILTER_SANITIZE_STRING);
        $merAmount = filter_input(INPUT_POST, 'merAmount', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);


        $query = "UPDATE Merchandise SET Customer = '".$merCust."', Merchandise = '".$merchandise."', Description = '".$merDesc."', Date = '".$merDate."', PaymentType = '".$merMethod."', NetCollected = ".$merAmount." WHERE `MerchandiseID` = ". $mercID;
        if ($mysqli->query($query) === TRUE) {
            //header('Location: ../adm/user.php?usrid='.$userID.'?err=SUCCESS');
            return "Merchandise information successfully Updated.";
        } else {
            //header('Location: ../adm/user.php?usrid='.$userID.'?err=FAILURE');
            return "ERROR: Merchandise information could not be Updated.";
        }
    } else {return "";}
}

// AGREGAR APPOINTMENT
function addAppointment($mysqli){
    if (isset($_POST["appCust"], $_POST['appReason'], $_POST['appDate'], $_POST['appTime'], $_POST['appShow'],$_POST['appNewMemb'])) {
        // Sanitize and validate the data passed in
        $instructorID = $_SESSION['user_id'];
        $appCust = filter_input(INPUT_POST, 'appCust', FILTER_SANITIZE_STRING);
		$appPhone = filter_input(INPUT_POST, 'appPhone', FILTER_SANITIZE_STRING);
		$appEmail = filter_input(INPUT_POST, 'appEmail', FILTER_SANITIZE_STRING);
        $appReason = filter_input(INPUT_POST, 'appReason', FILTER_SANITIZE_STRING);
        $appDate = date_create_from_format('m/d/Y', $_POST['appDate'])->format('Y-m-d')." ".$_POST['appTime'];
        $appShow = filter_input(INPUT_POST, 'appShow', FILTER_SANITIZE_NUMBER_INT);
        $appNewMemb = filter_input(INPUT_POST, 'appNewMemb', FILTER_SANITIZE_NUMBER_INT);

        // Insert the new school into the database
        $query = "INSERT INTO Appointments(Customer, Phone, Email, Reason, Date, ShowUp, NewMember, InstructorID) VALUES ('$appCust', '$appPhone', '$appEmail', '$appReason', '$appDate', '$appShow', '$appNewMemb', $instructorID)";
        if ($mysqli->query($query) === TRUE) {
            return "Appointment added to the database.";
        } else {
            return "ERROR: Unable to add Appointment.";
        }
    } else {return "";}
}

//LISTAR APPOINTMENTS
function listAppointments($mysqli){
    $per_page=20;
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page=1;
    }
    $start_from = ($page-1) * $per_page;

    $usrid = $_SESSION['user_id'];
    $checkQuery = "SELECT `role`,`username`  FROM `Users` WHERE `user_id` = ".$usrid;
    $userData = $mysqli->query($checkQuery);
    $userRow = $userData->fetch_assoc();
    $userRole = $userRow['role'];
    $username = $userRow['username'];

    if($userRole == "administrator") {
        $query = "SELECT `AppointmentID`, `Customer`, `Reason`, `Date`, `ShowUp`, `NewMember`, `Users`.`username` FROM `Appointments` INNER JOIN `Users` ON `InstructorID` = `Users`.`user_id` WHERE `Appointments`.Deleted = 0  ORDER BY `Date` DESC LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Appointments` INNER JOIN `Users` ON `InstructorID` = `Users`.`user_id` WHERE `Appointments`.Deleted = 0";
    } else {
        $query = "SELECT `AppointmentID`, `Customer`, `Reason`, `Date`, `ShowUp`, `NewMember`, `Users`.`username` FROM `Appointments` INNER JOIN `Users` ON `InstructorID` = `Users`.`user_id` WHERE `InstructorID` = ".$usrid." AND `Appointments`.Deleted = 0 ORDER BY `Date` DESC LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Appointments` INNER JOIN `Users` ON `InstructorID` = `Users`.`user_id` WHERE `InstructorID` = ".$usrid." AND `Appointments`.Deleted = 0";
    }
    $appointments = $mysqli->query($query);

    $app_list = '<table class="table" id="appTable">
                <thead>
                    <tr style="background-color: #313131; color: #fff ">
                        <th data-sort="string">
                            Customer <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Reason <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="date">
                            Date <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Show up <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="int">
                            New Member <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Instructor <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>';
    while (($row = $appointments->fetch_array()) != null)
    {
        $appID = $row['AppointmentID'];
        $appCustomer = $row['Customer'];
        $appReason = $row['Reason'];
        $appDateTime = explode(" ", $row['Date']);
        $appTime = date("H:i", strtotime($appDateTime[1]));
        $appDate = date_create_from_format('Y-m-d', $appDateTime[0])->format('m/d/Y');

        if($row['ShowUp'] == 1) {
            $appShow = 'Yes';
        } else {$appShow = 'No';}
        if($row['NewMember'] == 1) {
            $appNewMemb = 'Yes';
        } else {$appNewMemb = 'No';}

        $instructorID = $row['username'];

        $app_list .= '<tr class="rpt-member">';
        $app_list .= '<td><label><a href="appointment.php?appid='.$appID.'">'.$appCustomer.'</a></label></td>';
        $app_list .= '<td><label>'.$appReason.'</label></td>';
        $app_list .= '<td><label>'.$appDate.' - '.$appTime.'</label></td>';
        $app_list .= '<td><label>'.$appShow.'</label></td>';
        $app_list .= '<td><label>'.$appNewMemb.'</label></td>';
        $app_list .= '<td><label>'.$instructorID.'</label></td>';
        $app_list .= '</tr>';
    }
    $app_list .= '</tbody></table>';

    //PAGINATION

    $totalData = $mysqli->query($totalsQuery);
    $totalRows = $totalData->fetch_assoc();
    $total_pages = ceil($totalRows["row_num"] / $per_page);

    $pagination = '<div class="col-sm-12 col-xs-12 col-md-12 pagination-container"><nav><ul class="pagination">';
    $pagination .= '<li><a href="appointments.php?page=1" aria-label="Previous"><span aria-hidden="true">First</span></a></li>';

    for ($i=1; $i<=$total_pages; $i++) {
        $pagination .= '<li><a href="appointments.php?page='.$i.'">'.$i.'</a></li>';
    };

    $pagination .= '<li><a href="appointments.php?page='.$total_pages.'" aria-label="Previous"><span aria-hidden="true">Last</span></a></li>';
    $pagination .= '</ul></nav></div>';

    return $app_list.$pagination;
}

// UPDATE APPOINTMENT DATA
function updateAppData($mysqli){
    if (isset($_POST["appCust"], $_POST['appReason'], $_POST['appDate'], $_POST['appTime'], $_POST['appShow'],$_POST['appNewMemb'])) {
        // Sanitize and validate the data passed in
        $appID = $_POST['app_id'];
        $appCust = filter_input(INPUT_POST, 'appCust', FILTER_SANITIZE_STRING);
        $appReason = filter_input(INPUT_POST, 'appReason', FILTER_SANITIZE_STRING);
        $appDate = date_create_from_format('m/d/Y', $_POST['appDate'])->format('Y-m-d')." ".$_POST['appTime'];
        $appShow = filter_input(INPUT_POST, 'appShow', FILTER_SANITIZE_NUMBER_INT);
        $appNewMemb = filter_input(INPUT_POST, 'appNewMemb', FILTER_SANITIZE_NUMBER_INT);


        $query = "UPDATE Appointments SET Customer = '".$appCust."', Reason = '".$appReason."', Date = '".$appDate."', ShowUp = ".$appShow.", NewMember = ".$appNewMemb." WHERE `AppointmentID` = ". $appID;
        if ($mysqli->query($query) === TRUE) {
            //header('Location: ../adm/user.php?usrid='.$userID.'?err=SUCCESS');
            return "Appointment information successfully Updated.";
        } else {
            //header('Location: ../adm/user.php?usrid='.$userID.'?err=FAILURE');
            return "ERROR: Appointment information could not be Updated.";
        }
    } else {return "";}
}

//AGREGAR BILLING
function addBilling($mysqli){
    if (isset($_POST["billAmnt"], $_POST['billDate'],$_POST['billDesc'])) {
        // Sanitize and validate the data passed in
        $instructorID = $_SESSION['user_id'];
        $billDesc = filter_input(INPUT_POST, 'billDesc', FILTER_SANITIZE_STRING);
        $billDate = date_create_from_format('m/d/Y', $_POST['billDate'])->format('Y-m-d');
        $billAmount = filter_input(INPUT_POST, 'billAmnt', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        // Insert the new school into the database
        $query = "INSERT INTO Billing(amount, billing_date, description, instructor_id) VALUES ($billAmount, '$billDate', '$billDesc', $instructorID)";
        if ($mysqli->query($query) === TRUE) {
            return "Billing added to the database.";
        } else {
            return "ERROR: Unable to add Billing to the database.";
        }
    } else {return "";}
}

//LISTAR BILLINGS
function listBilling($mysqli){
    $per_page=20;
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page=1;
    }
    $start_from = ($page-1) * $per_page;

    $usrid = $_SESSION['user_id'];
    $checkQuery = "SELECT `role`,`username`  FROM `Users` WHERE `user_id` = ".$usrid;
    $userData = $mysqli->query($checkQuery);
    $userRow = $userData->fetch_assoc();
    $userRole = $userRow['role'];
    $username = $userRow['username'];

    if($userRole == "administrator") {
        $query = "SELECT `billing_id`, `amount`, `billing_date`, `description`, `School`.`Name` FROM `Billing` INNER JOIN `Users` ON `instructor_id` = `Users`.`user_id` INNER JOIN `School` ON `Users`.`school` = `School`.`SchoolID` ORDER BY `billing_date` DESC LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Billing` INNER JOIN `Users` ON `instructor_id` = `Users`.`user_id`";
    } else {
        $query = "SELECT `billing_id`, `amount`, `billing_date`, `description`, `School`.`Name` FROM `Billing` INNER JOIN `Users` ON `instructor_id` = `Users`.`user_id` INNER JOIN `School` ON `Users`.`school` = `School`.`SchoolID` WHERE `instructor_id` = ".$usrid." ORDER BY `billing_date` DESC LIMIT $start_from, $per_page";
        $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Billing` INNER JOIN `Users` ON `instructor_id` = `Users`.`user_id` WHERE `instructor_id` = ".$usrid;
    }
    $billings = $mysqli->query($query);

    $app_list = '<table class="table" id="mercTable">
                <thead>
                    <tr style="background-color: #313131; color: #fff ">
                        <th data-sort="string">
                            Billing ID <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Amount <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="date">
                            Date <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Description <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            School <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>';
    while (($row = $billings->fetch_array()) != null)
    {
        $billingID = $row['billing_id'];
        $billingAmount = $row['amount'];
        $billingDesc = $row['description'];
        $billingDate = date_create_from_format('Y-m-d', $row['billing_date'])->format('m/d/Y');

        //$instructorID = $row['username'];
        $schoolName = $row['Name'];

        $app_list .= '<tr class="rpt-member">';
        $app_list .= '<td><label>'.$billingID.'</label></td>';
        $app_list .= '<td><label>'.$billingAmount.'</label></td>';
        $app_list .= '<td><label>'.$billingDate.'</label></td>';
        $app_list .= '<td><label>'.$billingDesc.'</label></td>';
        $app_list .= '<td><label>'.$schoolName.'</label></td>';
        $app_list .= '</tr>';
    }
    $app_list .= '</tbody></table>';

    //PAGINATION

    $totalData = $mysqli->query($totalsQuery);
    $totalRows = $totalData->fetch_assoc();
    $total_pages = ceil($totalRows["row_num"] / $per_page);

    $pagination = '<div class="col-sm-12 col-xs-12 col-md-12 pagination-container"><nav><ul class="pagination">';
    $pagination .= '<li><a href="billing.php?page=1" aria-label="Previous"><span aria-hidden="true">First</span></a></li>';

    for ($i=1; $i<=$total_pages; $i++) {
        $pagination .= '<li><a href="billing.php?page='.$i.'">'.$i.'</a></li>';
    };

    $pagination .= '<li><a href="billing.php?page='.$total_pages.'" aria-label="Previous"><span aria-hidden="true">Last</span></a></li>';
    $pagination .= '</ul></nav></div>';

    return $app_list.$pagination;
}

//MASTER REPORT
function masterReport($mysqli, $iniDate, $endDate){

    //Listar escuelas
    $query = "SELECT `SchoolID`, `Name` FROM `School`";
    $schools = $mysqli->query($query);

    $arrSchools = array();
    while (($row = $schools->fetch_array()) != null)
    {
        array_push($arrSchools,$row);
    }
    $result = '';
    //Obtener Datos relativos a las escuelas
    foreach ($arrSchools as $school) {
        $result .= '<tr class="rpt-member '.$school["Name"].'">';

        $transactionQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason <> 'testing' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $billingQuery = "SELECT SUM(amount) FROM `Billing` INNER JOIN Users ON Users.user_id = Billing.instructor_id  WHERE Users.school = ".$school["SchoolID"]." AND (Billing.billing_date BETWEEN '".$iniDate."' AND '".$endDate."')";
        $expenseQuery = "SELECT SUM(Amount) FROM `Expenses` WHERE SchoolID = ".$school["SchoolID"]." AND (Date BETWEEN '".$iniDate."' AND '".$endDate."')";
        $renewalQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'renewal' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $membMercQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'merchandise' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $newMAmountQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'new' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $upgradeQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'upgrade' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $cashoutQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'cash-out' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $privateQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'private-class' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $otherQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'other' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $testingQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'testing' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $prepaidQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'pre-paid-test' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $newMembersQuery = "SELECT COUNT(MemberFName) FROM Members WHERE SchoolID = ".$school["SchoolID"]." AND (Members.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $merchandiseQuery = "SELECT SUM(NetCollected) FROM `Merchandise` INNER JOIN Users ON Users.user_id = Merchandise.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND (Merchandise.Date BETWEEN '".$iniDate."' AND '".$endDate."')";

        $billingDB = $mysqli->query($billingQuery);
        $billing = $billingDB->fetch_assoc();

        $transactionDB = $mysqli->query($transactionQuery);
        $transaction = $transactionDB->fetch_assoc();

        $expenseDB = $mysqli->query($expenseQuery);
        $expenses = $expenseDB->fetch_assoc();

        $renewalDB = $mysqli->query($renewalQuery);
        $renewal = $renewalDB->fetch_assoc();

        $newMAmountDB = $mysqli->query($newMAmountQuery);
        $newMAmount = $newMAmountDB->fetch_assoc();

        $upgradeDB = $mysqli->query($upgradeQuery);
        $upgrade = $upgradeDB->fetch_assoc();

        $cashoutDB = $mysqli->query($cashoutQuery);
        $cashout = $cashoutDB->fetch_assoc();

        $privateDB = $mysqli->query($privateQuery);
        $private = $privateDB->fetch_assoc();

        $otherDB = $mysqli->query($otherQuery);
        $other = $otherDB->fetch_assoc();

        $testingDB = $mysqli->query($testingQuery);
        $testing = $testingDB->fetch_assoc();

        $prepaidDB = $mysqli->query($prepaidQuery);
        $prepaid = $prepaidDB->fetch_assoc();

        $newMembDB = $mysqli->query($newMembersQuery);
        $newMemb = $newMembDB->fetch_assoc();

        $membMercDB = $mysqli->query($membMercQuery);
        $membMerc = $membMercDB->fetch_assoc();

        $mercDB = $mysqli->query($merchandiseQuery);
        $merchandise = $mercDB->fetch_assoc();

        $mercTotal = floatval($membMerc["SUM(Paid)"]) + floatval($merchandise["SUM(NetCollected)"]);

        $rnwUpgCout = floatval($renewal["SUM(Paid)"]) + floatval($upgrade["SUM(Paid)"]) + floatval($cashout["SUM(Paid)"]);
        //$billingTotal = floatval($billing["SUM(Paid)"]) + floatval($merchandise["SUM(NetCollected)"]);
        $billingTotal = floatval($billing["SUM(amount)"]);
        $production = floatval($transaction["SUM(Paid)"]);

        $result .= "<td>".$school["Name"]."</td>";
        $result .= "<td>".number_format($billingTotal,2)."</td>";
        $result .= "<td>".$newMemb["COUNT(MemberFName)"]."</td>";
        $result .= "<td>".number_format($newMAmount["SUM(Paid)"],2)."</td>";
        $result .= "<td>".number_format($rnwUpgCout,2)."</td>";
        $result .= "<td>".number_format($private["SUM(Paid)"],2)."</td>";
        $result .= "<td>".number_format($other["SUM(Paid)"],2)."</td>";
        $result .= "<td>".number_format($mercTotal,2)."</td>";
        $result .= "<td>".number_format($testing["SUM(Paid)"],2)."</td>";
        $result .= "<td>".number_format($prepaid["SUM(Paid)"],2)."</td>";
        $result .= "<td>".number_format($production,2)."</td>";

        $result .= "</tr>";
    }
    return $result;
}

//MEMBER REPORT
function memberReport($mysqli, $iniDate, $endDate){

    //Listar escuelas
    $query = "SELECT `SchoolID`, `Name` FROM `School`";
    $schools = $mysqli->query($query);

    $arrSchools = array();
    while (($row = $schools->fetch_array()) != null)
    {
        array_push($arrSchools,$row);
    }
    $result = '';
    //Obtener Datos relativos a las escuelas
    foreach ($arrSchools as $school) {
        $result .= '<tr class="rpt-member '.$school["Name"].'">';

        $appointmentsQuery = "SELECT COUNT(Customer) FROM `Appointments` INNER JOIN Users ON Users.user_id = Appointments.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Appointments.Deleted = 0 AND (Appointments.Date BETWEEN '".$iniDate."' AND '".$endDate."')";
        $showupQuery = "SELECT COUNT(Customer) FROM `Appointments` INNER JOIN Users ON Users.user_id = Appointments.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Appointments.Deleted = 0 AND Appointments.ShowUP = 1 AND (Appointments.Date BETWEEN '".$iniDate."' AND '".$endDate."')";
        $appNewMembQuery = "SELECT COUNT(Customer) FROM `Appointments` INNER JOIN Users ON Users.user_id = Appointments.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Appointments.Deleted = 0 AND Appointments.NewMember = 1 AND (Appointments.Date BETWEEN '".$iniDate."' AND '".$endDate."')";
        $studentQuery = "SELECT COUNT(MemberFName) FROM Members WHERE Deleted = 0 AND SchoolID = ".$school["SchoolID"];
        $mercQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID INNER JOIN Members ON Transaction.MemberID = Members.MemberID WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'merchandise' AND (Transaction.EntryDate >= Members.EntryDate AND Transaction.EntryDate <= Members.EntryDate + INTERVAL 1 MONTH)";

        $appDB = $mysqli->query($appointmentsQuery);
        $apointments = $appDB->fetch_assoc();

        $newMembValDB = $mysqli->query($mercQuery);
        $newMembVal = $newMembValDB->fetch_assoc();

        $appNewDB = $mysqli->query($appNewMembQuery);
        $appNewMember = $appNewDB->fetch_assoc();

        $showupDB = $mysqli->query($showupQuery);
        $showup = $showupDB->fetch_assoc();

        $studentDB = $mysqli->query($studentQuery);
        $activeStudents = $studentDB->fetch_assoc();

        $newMembValue = floatval($newMembVal["SUM(Paid)"]);

        //$newMembDB = $mysqli->query($newMembersQuery);
        //$newMemb = $newMembDB->fetch_assoc();

        if(floatval($showup["COUNT(Customer)"]) > 0){
            $showupRate = (floatval($showup["COUNT(Customer)"])/floatval($apointments["COUNT(Customer)"]))*100;
        } else {$showupRate = 0;}

        if(floatval($apointments["COUNT(Customer)"]) > 0){
            $convertRate = (floatval($appNewMember["COUNT(Customer)"])/floatval($apointments["COUNT(Customer)"]))*100;
        } else {$convertRate = 0;}

        $result .= "<td>".$school["Name"]."</td>";
        $result .= "<td>".$apointments["COUNT(Customer)"]."</td>";
        $result .= "<td>".$showup["COUNT(Customer)"]."</td>";
        $result .= "<td>".$showupRate."%</td>";
        $result .= "<td>".$appNewMember["COUNT(Customer)"]."</td>";
        $result .= "<td>".$convertRate."%</td>";
        $result .= "<td>".number_format($newMembValue,2)."</td>";
        $result .= "<td>".$activeStudents["COUNT(MemberFName)"]."</td>";

        $result .= "</tr>";
    }
    return $result;
}

//MASTER REPORT CHART
function masterReportChart($mysqli, $iniDate, $endDate){

    //Listar escuelas
    $query = "SELECT `SchoolID`, `Name` FROM `School`";
    $schools = $mysqli->query($query);

    $arrSchools = array();
    $arrayResult = array();

    while (($row = $schools->fetch_array()) != null)
    {
        array_push($arrSchools,$row);
    }
    $result = '';
    //Obtener Datos relativos a las escuelas
    foreach ($arrSchools as $school) {

        //$billingQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $billingQuery = "SELECT SUM(amount) FROM `Billing` INNER JOIN Users ON Users.user_id = Billing.instructor_id  WHERE Users.school = ".$school["SchoolID"]." AND (Billing.billing_date BETWEEN '".$iniDate."' AND '".$endDate."')";
        $renewalQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'renewal' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $upgradeQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'upgrade' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $cashoutQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'cash-out' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $privateQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'private-class' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $otherQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'other' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $testingQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'testing' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $prepaidQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND Transaction.Reason = 'pre-paid-test' AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $newMembersQuery = "SELECT COUNT(MemberFName) FROM Members WHERE SchoolID = ".$school["SchoolID"]." AND (Members.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $merchandiseQuery = "SELECT SUM(NetCollected) FROM `Merchandise` INNER JOIN Users ON Users.user_id = Merchandise.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND (Merchandise.Date BETWEEN '".$iniDate."' AND '".$endDate."')";

        $billingDB = $mysqli->query($billingQuery);
        $billing = $billingDB->fetch_assoc();

        $renewalDB = $mysqli->query($renewalQuery);
        $renewal = $renewalDB->fetch_assoc();

        $upgradeDB = $mysqli->query($upgradeQuery);
        $upgrade = $upgradeDB->fetch_assoc();

        $cashoutDB = $mysqli->query($cashoutQuery);
        $cashout = $cashoutDB->fetch_assoc();

        $privateDB = $mysqli->query($privateQuery);
        $private = $privateDB->fetch_assoc();

        $otherDB = $mysqli->query($otherQuery);
        $other = $otherDB->fetch_assoc();

        $testingDB = $mysqli->query($testingQuery);
        $testing = $testingDB->fetch_assoc();

        $prepaidDB = $mysqli->query($prepaidQuery);
        $prepaid = $prepaidDB->fetch_assoc();

        $newMembDB = $mysqli->query($newMembersQuery);
        $newMemb = $newMembDB->fetch_assoc();

        $mercDB = $mysqli->query($merchandiseQuery);
        $merchandise = $mercDB->fetch_assoc();

        $rnwUpgCout = floatval($renewal["SUM(Paid)"]) + floatval($upgrade["SUM(Paid)"]) + floatval($cashout["SUM(Paid)"]);
        //$billingTotal = floatval($billing["SUM(Paid)"]) + floatval($merchandise["SUM(NetCollected)"]);
        $billingTotal = floatval($billing["SUM(amount)"]);

        $data = array(
            "school_name" => $school["Name"],
            "billing" => intval($billingTotal),
            "new_member" => intval($newMemb["COUNT(MemberFName)"]),
            "rnw_upg_cout" => intval($rnwUpgCout),
            "private" => intval($private["SUM(Paid)"]),
            "other" => intval($other["SUM(Paid)"]),
            "merchandise" => intval($merchandise["SUM(NetCollected)"]),
            "testing" => intval($testing["SUM(Paid)"]),
            "prepaid" => intval($prepaid["SUM(Paid)"])
        );

        array_push($arrayResult, $data);
    }
    return $arrayResult;
}

//COMPANY REPORT
function companyReport($mysqli, $iniDate, $endDate){

    //Listar escuelas
    $query = "SELECT `SchoolID`, `Name` FROM `School`";
    $schools = $mysqli->query($query);

    $arrSchools = array();
    while (($row = $schools->fetch_array()) != null)
    {
        array_push($arrSchools,$row);
    }
    $result = '';
    //Obtener Datos relativos a las escuelas
    foreach ($arrSchools as $school) {
        $result .= '<tr class="rpt-member '.$school["Name"].'">';

        $incomeQuery = "SELECT SUM(Paid) FROM `Transaction` INNER JOIN Users ON Users.user_id = Transaction.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND (Transaction.EntryDate BETWEEN '".$iniDate."' AND '".$endDate."')";
        $billingQuery = "SELECT SUM(amount) FROM `Billing` INNER JOIN Users ON Users.user_id = Billing.instructor_id  WHERE Users.school = ".$school["SchoolID"]." AND (Billing.billing_date BETWEEN '".$iniDate."' AND '".$endDate."')";
        $expenseQuery = "SELECT SUM(Amount) FROM `Expenses` WHERE SchoolID = ".$school["SchoolID"]." AND (Date BETWEEN '".$iniDate."' AND '".$endDate."')";
        $merchandiseQuery = "SELECT SUM(NetCollected) FROM `Merchandise` INNER JOIN Users ON Users.user_id = Merchandise.InstructorID  WHERE Users.school = ".$school["SchoolID"]." AND (Merchandise.Date BETWEEN '".$iniDate."' AND '".$endDate."')";

        $incomeDB = $mysqli->query($incomeQuery);
        $income = $incomeDB->fetch_assoc();

        $billingDB = $mysqli->query($billingQuery);
        $billing = $billingDB->fetch_assoc();

        $expenseDB = $mysqli->query($expenseQuery);
        $expenses = $expenseDB->fetch_assoc();

        $mercDB = $mysqli->query($merchandiseQuery);
        $merchandise = $mercDB->fetch_assoc();

        $billingTotal = floatval($income["SUM(Paid)"]) + floatval($merchandise["SUM(NetCollected)"]) + floatval($billing["SUM(amount)"]);
        $production = $billingTotal - floatval($expenses["SUM(Amount)"]);

        $result .= "<td>".$school["Name"]."</td>";
        $result .= "<td>".number_format($billingTotal,2)."</td>";
        $result .= "<td>- ".number_format($expenses["SUM(Amount)"],2)."</td>";
        $result .= "<td>".number_format($production,2)."</td>";

        $result .= "</tr>";
    }
    return $result;
}

//AGREGAR EXPENSES
function addExpense($mysqli){
    if (isset($_POST["expSchool"], $_POST['expReason'], $_POST['expDesc'],$_POST['expDate'],$_POST['expAmount'])) {
        // Sanitize and validate the data passed in
        $expSchool = filter_input(INPUT_POST, 'expSchool', FILTER_SANITIZE_NUMBER_INT);
        $expReason = filter_input(INPUT_POST, 'expReason', FILTER_SANITIZE_STRING);
        $expDesc = filter_input(INPUT_POST, 'expDesc', FILTER_SANITIZE_STRING);
        $expDate = date_create_from_format('m/d/Y', $_POST['expDate'])->format('Y-m-d');
        $expAmount = filter_input(INPUT_POST, 'expAmount', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        // Insert the new school into the database
        $query = "INSERT INTO Expenses(SchoolID, Reason, Description, Date, Amount) VALUES ('$expSchool', '$expReason', '$expDesc', '$expDate', '$expAmount')";
        if ($mysqli->query($query) === TRUE) {
            return "Expense added to the database.";
        } else {
            return "ERROR: Unable to add Expense.";
        }
    } else {return "";}
}

//LISTAR EXPENSES
function listExpenses($mysqli){
    $per_page=20;
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page=1;
    }
    $start_from = ($page-1) * $per_page;

    $query = "SELECT `Expenses`.`Amount`, `Expenses`.`Reason`, `Expenses`.`Date`, `Expenses`.`Description`,`School`.`Name` AS `schoolname` FROM `Expenses` INNER JOIN `School` ON `School`.`SchoolID` = `Expenses`.`SchoolID` ORDER BY `Expenses`.`Date` DESC LIMIT $start_from, $per_page";
    $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Expenses` INNER JOIN `School` ON `School`.`SchoolID` = `Expenses`.`SchoolID`";

    $expenses = $mysqli->query($query);

    $app_list = '<table class="table" id="mercTable">
                <thead>
                    <tr style="background-color: #313131; color: #fff ">
                        <th data-sort="string">
                            School <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Reason <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="date">
                            Date <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            Description <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="int">
                            Amount <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>';
    while (($row = $expenses->fetch_array()) != null)
    {
        $expSchool = $row['schoolname'];
        $expReason = $row['Reason'];
        $expDesc = $row['Description'];
        $appAmount = $row['Amount'];
        $expDate = date_create_from_format('Y-m-d', $row['Date'])->format('m/d/Y');

        $app_list .= '<tr class="rpt-member">';
        $app_list .= '<td><label>'.$expSchool.'</label></td>';
        $app_list .= '<td><label>'.$expReason.'</label></td>';
        $app_list .= '<td><label>'.$expDate.'</label></td>';
        $app_list .= '<td><label>'.$expDesc.'</label></td>';
        $app_list .= '<td><label>'.number_format($appAmount,2).'</label></td>';
        $app_list .= '</tr>';
    }
    $app_list .= '</tbody></table>';

    //PAGINATION

    $totalData = $mysqli->query($totalsQuery);
    $totalRows = $totalData->fetch_assoc();
    $total_pages = ceil($totalRows["row_num"] / $per_page);

    $pagination = '<div class="col-sm-12 col-xs-12 col-md-12 pagination-container"><nav><ul class="pagination">';
    $pagination .= '<li><a href="expenses.php?page=1" aria-label="Previous"><span aria-hidden="true">First</span></a></li>';

    for ($i=1; $i<=$total_pages; $i++) {
        $pagination .= '<li><a href="expenses.php?page='.$i.'">'.$i.'</a></li>';
    };

    $pagination .= '<li><a href="expenses.php?page='.$total_pages.'" aria-label="Previous"><span aria-hidden="true">Last</span></a></li>';
    $pagination .= '</ul></nav></div>';

    return $app_list.$pagination;
}

function searchMembers($mysqli, $fname, $lname, $activo){
    $per_page=20;
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page=1;
    }
    $start_from = ($page-1) * $per_page;

    $usrid = $_SESSION['user_id'];
    $checkQuery = "SELECT `role`, `school` FROM `Users` WHERE `user_id` = ".$usrid;
    $userData = $mysqli->query($checkQuery);
    $userRow = $userData->fetch_assoc();
    $userRole = $userRow['role'];
    $userSchool = $userRow['school'];

    if($userRole == "administrator") {
        if($fname == "" or $lname == ""){
            $query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `Members`.`Type`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = $activo AND (MemberFName = '".$fname."' OR MemberLName = '".$lname."') ORDER BY `MemberLName` LIMIT $start_from, $per_page";
            $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = $activo AND (MemberFName = '".$fname."' OR MemberLName = '".$lname."')";
        }
        else {
            $query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `Members`.`Type`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = $activo AND (MemberFName = '".$fname."' AND MemberLName = '".$lname."') ORDER BY `MemberLName` LIMIT $start_from, $per_page";
            $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = $activo AND (MemberFName = '".$fname."' AND MemberLName = '".$lname."')";
        }

    } else {
        if($fname == "" or $lname == ""){
            $query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `Members`.`Type`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = $activo AND `Members`.`SchoolID` = ".$userSchool." AND (MemberFName = '".$fname."' OR MemberLName = '".$lname."') ORDER BY `MemberLName` LIMIT $start_from, $per_page";
            $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = $activo AND (MemberFName = '".$fname."' OR MemberLName = '".$lname."') AND `Members`.`SchoolID` = ".$userSchool;
        }
        else {
            $query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `Members`.`Type`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = $activo AND `Members`.`SchoolID` = ".$userSchool." AND (MemberFName = '".$fname."' AND MemberLName = '".$lname."') ORDER BY `MemberLName` LIMIT $start_from, $per_page";
            $totalsQuery = "SELECT COUNT(*) AS row_num FROM `Members` INNER JOIN `School` ON `School`.`SchoolID` = `Members`.`SchoolID` WHERE Members.Deleted = $activo AND (MemberFName = '".$fname."' AND MemberLName = '".$lname."') AND `Members`.`SchoolID` = ".$userSchool;
        }

    }

    //$query = "SELECT `Members`.`MemberID`, `MemberFName`, `MemberLName`, `EntryDate`, `School`.`Name` AS `schoolname` FROM `Members` INNER JOIN `School` ON `SchoolID` = `Members`.`SchoolID`";
    $users = $mysqli->query($query);

    $member_list = '<table class="table" id="simpleTable"><thead><tr style="background-color: #313131; color: #fff ">';
    $member_list .= '<th data-sort="string">Member <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">Type <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">School <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '<th data-sort="string">Entry Date <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i></th>';
    $member_list .= '</tr></thead><tbody>';

    while (($row = $users->fetch_array()) != null)
    {
        $memberID = $row['MemberID'];
        $MemberFName = $row['MemberFName'];
        $MemberLName = $row['MemberLName'];
        $MemberType = $row['Type'];
        $EntryDate = date_create_from_format('Y-m-d', $row['EntryDate'])->format('m/d/Y');
        $schoolname = $row['schoolname'];

        $member_list .= '<tr class="rpt-member">';
        $member_list .= '<td><label><a href="details.php?usrid='.$memberID.'">'.$MemberFName.' '.$MemberLName.'</a></label></td>';
        $member_list .= '<td><label>'.$MemberType.'</label></td>';
        $member_list .= '<td><label>'.$schoolname.'</label></td>';
        $member_list .= '<td><label>'.$EntryDate.'</label></td>';
        $member_list .= '</tr>';
    }
    $member_list .= '</tbody></table>';

    //PAGINATION

    $totalData = $mysqli->query($totalsQuery);
    $totalRows = $totalData->fetch_assoc();
    $total_pages = ceil($totalRows["row_num"] / $per_page);

    $pagination = '<div class="col-sm-12 col-xs-12 col-md-12 pagination-container"><nav><ul class="pagination">';
    $pagination .= '<li><a href="dashboard.php?page=1" aria-label="Previous"><span aria-hidden="true">First</span></a></li>';

    for ($i=1; $i<=$total_pages; $i++) {
        $pagination .= '<li><a href="dashboard.php?page='.$i.'">'.$i.'</a></li>';
    };

    $pagination .= '<li><a href="dashboard.php?page='.$total_pages.'" aria-label="Previous"><span aria-hidden="true">Last</span></a></li>';
    $pagination .= '</ul></nav></div>';

    return $member_list.$pagination;
}