<?php
include_once 'db_conn.php';

    if (isset($_POST['user_id'], $_POST['member_deleted'])) {
        $userID = $_POST['user_id'];

        $query = "UPDATE Members SET Members.`Deleted` = 1 WHERE `MemberID` = ". $userID;
        if ($conn->query($query) === TRUE) {
            header('Location: ../dashboard.php?err=deleted');
        } else {
            header('Location: ../dashboard.php?err=Failure');
        }
    }

elseif (isset($_POST['app_id'], $_POST['app_deleted'])) {
    $appID = $_POST['app_id'];

    $query = "UPDATE Appointments SET Appointments.`Deleted` = 1 WHERE `AppointmentID` = ". $appID;
    if ($conn->query($query) === TRUE) {
        header('Location: ../appointments.php?err=deleted');
    } else {
        header('Location: ../appointments.php?err=Failure');
    }
} else {return "";}
