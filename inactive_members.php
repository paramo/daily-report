<?php include 'header.php';?>
<?php if(isset($_GET['activate'])){
    $membID = $_GET['activate'];
    $query = "UPDATE Members SET Deleted = 0 WHERE `MemberID` = ". $membID;
    if ($conn->query($query) === TRUE) {
        //header('Location: ../adm/user.php?usrid='.$userID.'?err=SUCCESS');
        $errMsg = "Member has been changed to Active.";
    } else {
        //header('Location: ../adm/user.php?usrid='.$userID.'?err=FAILURE');
        $errMsg = "ERROR: Member information could not be Updated.";
    }
} ?>
<?php menu_sidebar(); ?>
         <div class="content">
            <div class="col-md-12">
               <h2>Inactive Members</h2>
                <div class="col-lg-6 col-sm-12">
                    <p>List of members by School. Select a member to access the details and manage transactions. </p>
                </div>
                <div class="col-lg-6 col-sm-12 text-right">
                    <a href="dashboard.php" class="boton-sm ami btn-user-update">Back to Active Members</a>
                </div>
            </div>
             <div class="col-md-12 col-lg-12">
                 <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="search_form">
                     <h3>Search Member</h3>
                     <div class="col-lg-3 col-sm-12">
                         <label for="fname">Member first name</label>
                         <input type="text" name="fName" id="fname" />
                     </div>
                     <div class="col-lg-3 col-sm-12">
                         <label for="lname">Member last name</label>
                         <input type="text" name="lName" id="lname" />
                     </div>
                     <div class="col-lg-1 col-sm-12">
                         <input type="submit" class="boton-sm ami btn-add-payment" value="Search" />
                     </div>
                     <div class="col-lg-5 col-sm-12">
                         <a class="boton-sm-grey ami btn-add-payment" href="inactive_members.php" role="button">Back</a>
                     </div>
                 </form>
             </div>
            <div class="col-md-12 col-lg-12">
                <?php if(isset($_GET['activate'])) echo '<p class="errMsg">'.$errMsg.'</p>';?>
               <div class="panel panel-default">
                  <!-- Default panel contents -->
                  <!--<div class="panel-heading">Panel heading</div>-->

                   <!-- Table -->
                   <?php if(isset($_POST["fName"]) || isset($_POST["lName"])){
                       echo searchMembers($conn, $_POST["fName"], $_POST["lName"], 1);
                   } else {
                       echo listInactiveMembers($conn);
                   } ?>
               </div>
            </div>

         </div>
<?php include 'footer.php'?>