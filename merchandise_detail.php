<?php
include 'header.php';
$updateMercMsg = updateMercData($conn);
//GET MERCHANDISE INFO
$merid = 0;
if(isset($_GET["merid"])) {
    $merid = $_GET["merid"];
}
elseif(isset($_POST["merid"])) {
    $merid = $_POST["merid"];
}
$query = "SELECT `Customer`, `Merchandise`, `Description`, `Date`, `PaymentType`, `NetCollected` FROM `Merchandise` WHERE `MerchandiseID` = ". $merid;
$merchandise = $conn->query($query);
$mercData = $merchandise->fetch_assoc();
$mercDate = date_create_from_format('Y-m-d', $mercData['Date'])->format('m/d/Y');
?>
<?php menu_sidebar(); ?>
<div class="content">
    <div class="col-md-12">
        <h2>Details</h2>
        <p class="breadcrumb">
            <a href="merchandise.php">Merchandise</a> > <label>Details</label>
        </p>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
          <form action="<?php echo esc_url($_SERVER['PHP_SELF'])."?merid=".$merid; ?>" method="post" name="merchandise_form">
            <div class="panel-body n-p-l-r">
                <input type="hidden" name="merc_id" value="<?php echo $merid; ?>" />
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p for="select-reason" style="font-weight: bold">
                        Customer
                    </p>
                    <input type="text" name="merCust" value="<?php echo $mercData["Customer"]; ?>" />
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Merchandise
                    </p>
                    <input type="text" name="merchandise" value="<?php echo $mercData["Merchandise"]; ?>" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Description (140 characters)
                    </p>
                    <input type="text" name="merDesc" value="<?php echo $mercData["Description"]; ?>" />
                </div>
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Date
                    </p>
                    <input type="text" name="merDate" value="<?php echo $mercDate; ?>" class="datepicker" id="datepicker-pay"/>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Payment Method
                    </p>
                    <select id="select-reason" name="merMethod">
                        <option value="cash" <?php if($mercData["PaymentType"] == "cash") echo 'selected="selected"'; ?>>Cash</option>
                        <option value="check" <?php if($mercData["PaymentType"] == "check") echo 'selected="selected"'; ?>>Check</option>
                        <option value="asf" <?php if($mercData["PaymentType"] == "asf") echo 'selected="selected"'; ?>>ASF</option>
                        <option value="cc" <?php if($mercData["PaymentType"] == "cc") echo 'selected="selected"'; ?>>CC</option>
                        <option value="other" <?php if($mercData["PaymentType"] == "other") echo 'selected="selected"'; ?>>Other</option>
                    </select>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Net Collected
                    </p>
                    <input type="text" name="merAmount" value="<?php echo $mercData["NetCollected"]; ?>" style="background-color: #F7E0E0; font-weight: bold" />
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <input type="button" class="boton-sm ami btn-add-payment" value="Save" onclick="merchandiseValidate(this.form, this.form.merCust, this.form.merchandise, this.form.merDate);" />
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <a class="boton-sm-grey ami btn-add-payment" href="#" role="button">Delete</a>
                </div>
                <p><?php echo $updateMercMsg; ?></p>
            </div>
          </form>
        </div>
    </div>
</div>
<?php include 'footer.php'?>
