<?php
include 'header.php';
$updateAppMsg = updateAppData($conn);
//GET MERCHANDISE INFO
$appid = 0;
if(isset($_GET["appid"])) {
    $appid = $_GET["appid"];
}
elseif(isset($_POST["appid"])) {
    $appid = $_POST["appid"];
}
$query = "SELECT `Customer`, `Reason`, `Date`, `ShowUp`, `NewMember` FROM `Appointments` WHERE `AppointmentID` = ". $appid;
$appointments = $conn->query($query);
$appData = $appointments->fetch_assoc();
$appDateTime = explode(" ", $appData['Date']);
$appTime = date("H:i", strtotime($appDateTime[1]));
$appDate = date_create_from_format('Y-m-d', $appDateTime[0])->format('m/d/Y');
?>
<?php menu_sidebar(); ?>
<div class="content">
    <div class="col-md-12">
        <h2>Appointment</h2>
        <p class="breadcrumb">
            <a href="appointments.php">Appointments</a> > <label><?php echo $appData["Customer"]; ?> - <?php echo $appDate; ?></label>
        </p>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <form action="<?php echo esc_url($_SERVER['PHP_SELF'])."?appid=".$appid; ?>" method="post" name="appointment_form">
                <input type="hidden" name="app_deleted" value="0" id="app_deleted">
            <div class="panel-body n-p-l-r">
                <input type="hidden" name="app_id" value="<?php echo $appid; ?>" />
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p for="select-reason" style="font-weight: bold">
                        Customer
                    </p>
                    <input type="text" name="appCust" value="<?php echo $appData["Customer"]; ?>" />
                </div>
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p for="select-reason" style="font-weight: bold">
                        Reason
                    </p>
                    <select id="select-reason" name="appReason">
                        <option value="" selected="selected" disabled="disabled">Select</option>
                        <option value="new">New Member</option>
                        <option value="cash-out">Cash Out</option>
                        <option value="renewal">Renewal</option>
                        <option value="upgrade">Upgrade</option>
                        <option value="private-class">Private Class</option>
                        <option value="testing">Testing</option>
                        <option value="pre-paid-test">Pre-paid testing</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Date
                    </p>
                    <input type="text" name="appDate" value="<?php echo $appDate; ?>" class="datepicker" id="datepicker-pay"/>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Time
                    </p>
                    <input type="text" name="appTime" value="<?php echo $appTime; ?>" class="timepicker" id="timepicker-app" data-time-format="H:i"/>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Show up
                    </p>
                    <select id="select-showup" name="appShow">
                        <option value="1" <?php if($appData["ShowUp"] == 1) echo 'selected="selected"'; ?>>Yes</option>
                        <option value="0" <?php if($appData["ShowUp"] == 0) echo 'selected="selected"'; ?>>No</option>
                    </select>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        New member
                    </p>
                    <select id="select-newmember" name="appNewMemb">
                        <option value="1" <?php if($appData["NewMember"] == 1) echo 'selected="selected"'; ?>>Yes</option>
                        <option value="0" <?php if($appData["NewMember"] == 0) echo 'selected="selected"'; ?>>No</option>
                    </select>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <input type="button" class="boton-sm ami btn-add-payment" value="Save" onclick="appointmentValidate(this.form, this.form.appCust, this.form.appDate);" />
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <input type="button" class="boton-sm-grey ami btn-add-payment" value="Delete" onclick="deleteAppoint(this.form);" />
                </div>
            </div>
                <p><?php echo $updateAppMsg; ?></p>
                </form>
        </div>
    </div>
    <script>
        function deleteAppoint(form){
            $('#app_deleted').val('deleted','1');
            $('form[name="appointment_form"]').attr("action", "includes/member_remove.php");
            form.submit();
        }
        $('#select-reason option[value="<?php echo $appData["Reason"] ?>"]').attr('selected', 'selected');
    </script>
</div>
<?php include 'footer.php'?>
