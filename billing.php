<?php
include 'header.php';
$billingMsg = addBilling($conn);
?>
<?php menu_sidebar(); ?>
<div class="content">
    <div class="col-md-12">
        <h2>Billing</h2>
        <p>
        </p>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="col-lg-4"><h3 class="panel-title">Add Billing</h3></div>
                <div class="col-lg-8 text-right"><span><?php echo $billingMsg; ?></span></div>
            </div>
            <div class="panel-body n-p-l-r">
                <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="billing_form">
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p for="select-reason" style="font-weight: bold">
                        Billing Amount
                    </p>
                    <input type="text" name="billAmnt" placeholder="" />
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Date
                    </p>
                    <input type="text" name="billDate" placeholder="10/24/2015" class="datepicker" id="datepicker-pay"/>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Description
                    </p>
                    <input type="text" name="billDesc" id="description"/>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <input type="button" class="boton-sm ami btn-add-payment" value="Add" onclick="billingValidate(this.form, this.form.billAmnt, this.form.billDate);" />
                </div>
                    </form>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">

            <!-- Table -->
                <?php echo listBilling($conn); ?>
        </div>
    </div>

</div>
<?php include 'footer.php'?>
