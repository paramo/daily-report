<?php
include_once 'includes/db_conn.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($conn) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!doctype html>
   <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
   <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
   <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
   <!--[if gt IE 8]><!--> 
   <html class="no-js" lang=""> 
   <!--<![endif]-->
      <head>
         <meta charset="utf-8">
         <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
         <title>Daily Report - Winners Martial Arts</title>
         <meta name="description" content="">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="apple-touch-icon" href="apple-touch-icon.png">
         <link rel="icon" href="favicon.png">
         <link rel="stylesheet" href="css/bootstrap.min.css">
         <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
         </style>
         <link rel="stylesheet" href="css/bootstrap-theme.min.css">
         <link rel="stylesheet" href="css/main.css">
         <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
         <script type="text/JavaScript" src="js/sha512.js"></script>
         <script type="text/JavaScript" src="js/forms.js"></script>
      </head>
      <body>
      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="container">
         <div class="panel panel-default login-panel">
            <img src="img/logo-simple.svg" id="login-logo">
            <div class="panel-body">
                <form action="includes/process_login.php" method="post" name="login_form">
               <div class="col-lg-12 login-form">
                  <p for="your_id">Your ID</p>
                  <input type="text" id="your_id" name="username">
               </div>
               <div class="col-lg-12">
                  <p for="password">Password</p>
                  <input type="password" id="password" name="password">
               </div>
               <div class="col-lg-12 login-button">
                   <input type="button" class="boton ani" value="ACCESS" onclick="formhash(this.form, this.form.password);" />
               </div>
               </form>
                <?php
                if (login_check($conn) == true) {
                    echo '<div class="col-lg-12 text-center">';
                    echo '<p>Currently logged ' . $logged . ' as ' . htmlentities($_SESSION['username']) . '.</p>';
                    echo '<p>Go to the <a href="dashboard.php">Dashboard</a>.</p>';
                    echo '<p>Do you want to change user? <a href="includes/logout.php">Log out</a>.</p>';
                    echo '</div>';
                }
                elseif ($_GET['error'] == 1) {
                    echo '<div class="col-lg-12 text-center">';
                    echo '<p class="red">Login Invalid, please try again.</p>';
                    echo '</div>';
                }
                elseif ($_GET['error'] == 2) {
                    echo '<div class="col-lg-12 text-center">';
                    echo '<p class="red">Account Locked, please contact your Web Administrator.</p>';
                    echo '</div>';
                }
                ?>
            </div>
         </div>
          <!-- Example row of columns -->
        <!--
        <div class="row">
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
         </div>
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
          </div>-->
        </div>
        
        <hr>
        <footer>
          <p>&copy; WinnersMA 2016</p>
        </footer>
      </div> 
      <!-- /container -->        
      <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
      <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
      <script src="js/vendor/bootstrap.min.js"></script>
      <script src="js/main.js"></script>
      <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
      <script>
          (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
          function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
          e=o.createElement(i);r=o.getElementsByTagName(i)[0];
          e.src='//www.google-analytics.com/analytics.js';
          r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
          ga('create','UA-XXXXX-X','auto');ga('send','pageview');
      </script>-->
    </body>
</html>
