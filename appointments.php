<?php
include 'header.php';
$appointMsg = addAppointment($conn);
?>
<?php menu_sidebar(); ?>
<div class="content">
    <div class="col-md-12">
        <h2>Appointments</h2>
        <p>
        </p>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="col-lg-4"><h3 class="panel-title">New Appointment</h3></div>
                <div class="col-lg-8 text-right"><span><?php echo $appointMsg; ?></span></div>
            </div>
            <div class="panel-body n-p-l-r">
                <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="appointment_form">
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p for="select-reason" style="font-weight: bold">
                        Customer
                    </p>
                    <input type="text" name="appCust" placeholder="" />
                </div>
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p for="select-reason" style="font-weight: bold">
                        Reason
                    </p>
                    <select id="select-reason" name="appReason">
                        <option value="" selected="selected" disabled="disabled">Select</option>
                        <option value="new">New Member</option>
                        <option value="cash-out">Cash Out</option>
                        <option value="renewal">Renewal</option>
                        <option value="upgrade">Upgrade</option>
                        <option value="private-class">Private Class</option>
                        <option value="testing">Testing</option>
                        <option value="pre-paid-test">Pre-paid testing</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Date
                    </p>
                    <input type="text" name="appDate" placeholder="10/24/2015" class="datepicker" id="datepicker-pay"/>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Time
                    </p>
                    <input type="text" name="appTime" placeholder="10:30" class="timepicker" id="timepicker-app" data-time-format="H:i"/>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Show up
                    </p>
                    <select id="select-showup" name="appShow">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        New member
                    </p>
                    <select id="select-newmember" name="appNewMemb">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
                    <input type="button" class="boton-sm ami btn-add-payment" value="Add" onclick="appointmentValidate(this.form, this.form.appCust, this.form.appDate);" />
                </div>
                    </form>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <?php if($_GET['err'] == 'deleted') echo '<p class="errMsg">Appointment Deleted</p>';?>

            <!-- Table -->
                <?php echo listAppointments($conn); ?>
        </div>
    </div>

</div>
<?php include 'footer.php'?>
