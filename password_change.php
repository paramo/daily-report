<?php
include_once 'includes/db_conn.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($conn) == true) :

    newPassword($conn);

    $query = "SELECT username, user_id FROM Users";
    $users = $conn->query($query);

    $users_select = "<select name = 'user'>";
    while (($row = $users->fetch_array()) != null)
    {
        $users_select .= "<option value = '{$row['user_id']}'";
        $users_select .= ">{$row['username']}</option>";
    }
    $users_select .= "</select>";

?>
    <script type="text/JavaScript" src="js/sha512.js"></script>
    <script type="text/JavaScript" src="js/forms.js"></script>

    <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>"
          method="post"
          name="change_password_form">
        Username: <?php echo $users_select; ?>
        New Password: <input type="password"
                         name="new_password"
                         id="new_password"/><br>
        Confirm password: <input type="password"
                                 name="confirmpwd"
                                 id="confirmpwd" /><br>
        <input type="button"
               value="Save Changes"
               onclick="return setNewPassword(this.form, this.form.new_password, this.form.confirmpwd);" />
    </form>
<?php else : ?>
    <p>
        <span class="error">You are not authorized to access this page.</span> Please <a href="index.php">login</a>.
    </p>
<?php endif; ?>