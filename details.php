<?php
include 'header.php';
$updateMemberMsg = updateMemberData($conn);
$transactionMsg = addPayment($conn);
//GET INSTRUCTOR INFO
$userid = 0;
if(isset($_GET["usrid"])) {
    $userid = $_GET["usrid"];
}
elseif(isset($_POST["user_id"])) {
    $userid = $_POST["user_id"];
}
$query = "SELECT `MemberID`, `MemberFName`, `MemberLName`, `Type`, `EntryDate` FROM `Members` WHERE `MemberID` = ". $userid;
$user = $conn->query($query);
$userData = $user->fetch_assoc();
$memberEntryDate = date_create_from_format('Y-m-d', $userData['EntryDate'])->format('m/d/Y');
?>
<?php menu_sidebar(); ?>
<div class="content">
   <div class="col-md-12">
      <h2><?php echo $userData['MemberFName']; ?> <?php echo $userData['MemberLName']; ?></h2>
      <p class="breadcrumb">
         <a href="dashboard.php">Members list</a> > <label>Details</label>
      </p>
   </div>
   <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" style="color: red;font-size: 1.2em; font-weight: bold; text-align: center">
      <?php echo $transactionMsg; ?>
      <?php echo $updateMemberMsg; ?>
   </div>
<!-- UPDATE MEMBER INFORMATION -->
   <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
      <div class="panel panel-default">
          <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="member_form">
              <input type="hidden" name="member_deleted" value="0" id="member_deleted">
         <div class="panel-heading">
            <h3 class="panel-title">Member information</h3>
         </div>
         <div class="panel-body n-p">
            <input type="hidden" name="user_id" value="<?php echo $userid; ?>" />
            <div class="row">
               <div class="col-xs-12 col-md-4">
                  <label style="margin-top: 8px">
                     First Name
                  </label>
                  <input type="text" name="member_fname" placeholder="John" value="<?php echo $userData['MemberFName']; ?>"/>
               </div>
               <div class="col-xs-12 col-md-4">
                  <label  style="margin-top: 8px">
                     Last Name
                  </label>
                  <input type="text" name="member_lname" placeholder="Smith" value="<?php echo $userData['MemberLName']; ?>"/>
               </div>
               <div class="col-xs-12 col-md-2">
                  <label  style="margin-top: 8px">
                     Entry Date
                  </label>
                  <input type="text" name="member_date" placeholder="10/24/2015" class="datepicker" id="datepicker-update" value="<?php echo $memberEntryDate; ?>"/>
               </div>
               <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                   <label for="select-type" style="margin-top: 8px">
                       Type
                   </label>
                   <select id="select-type" name="member_type">
                       <option value="" selected="selected" disabled="disabled">Select</option>
                       <option value="Trial-Program">Trial Program</option>
                       <option value="Master-Club">Master Club</option>
                       <option value="Leadership">Leadership</option>
                       <option value="Legacy">Legacy</option>
                       <option value="other">Other</option>
                   </select>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <p><input type="button" class="boton-sm ami btn-user-update" value="Update" onclick="memberValidate(this.form, this.form.member_fname, this.form.member_lname, this.form.member_date);" />
                      <input type="button" class="boton-sm-grey ami btn-user-update" value="Disable" onclick="deleteMember(this.form);" /></p>
               </div>
            </div>
         </div>
              </form>
      </div>
   </div>
   <!-- ADD TRANSACTION -->
   <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
      <div class="panel panel-default">
         <div class="panel-heading">
            <h3 class="panel-title">Add payment</h3>
         </div>
         <div class="panel-body n-p-l-r">
             <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="transaction_form">
                 <input type="hidden" name="user_id" value="<?php echo $userid; ?>" />
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
               <p for="select-reason" style="font-weight: bold">
                  Reason
               </p>
               <select id="select-reason" name="trnReason">
                  <option value="" selected="selected" disabled="disabled">Select</option>
                  <option value="new">New Member</option>
                  <option value="cash-out">Cash Out</option>
                  <option value="renewal">Renewal</option>
                  <option value="upgrade">Upgrade</option>
                  <option value="private-class">Private Class</option>
                  <option value="testing">Testing</option>
                  <option value="pre-paid-test">Pre-paid testing</option>
                  <option value="merchandise">Merchandise</option>
                  <option value="other">Other</option>
               </select>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
               <p style="font-weight: bold">
                  Description (140 characters)
               </p>
               <input type="text" name="trnDesc" placeholder="PRIVATE LESSONS" />
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
               <p style="font-weight: bold">
                  Date
               </p>
               <input type="text" placeholder="10/24/2015" class="datepicker" id="datepicker-pay" name="trnDate"/>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
               <p style="font-weight: bold">
                  Payment Method
               </p>
               <select id="select-method" name="trnMethod">
                  <option value="cash">Cash</option>
                  <option value="check">Check</option>
                  <option value="asf">ASF</option>
                  <option value="cc">CC</option>
                  <option value="other">Other</option>
               </select>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
               <p style="font-weight: bold">
                  Net Collected
               </p>
               <input type="text" name="trnAmount" placeholder="1200" style="background-color: #F7E0E0; font-weight: bold" />
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
               <input type="submit" class="boton-sm ami btn-add-payment" value="Add" />
                
            </div>
           </form>
         </div>
      </div>
   </div>

   <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
      <div class="panel panel-default">
         <!-- Default panel contents -->
         <!--<div class="panel-heading">Panel heading</div>-->

         <!-- Table -->

               <?php echo listTransactions($conn, $userid); ?>
      </div>
   </div>
<script>
    function deleteMember(form){
        $('#member_deleted').val('deleted','1');
        $('form[name="member_form"]').attr("action", "includes/member_remove.php");
        form.submit();
    }
    $('#select-type option[value="<?php echo $userData["Type"] ?>"]').attr('selected', 'selected');
</script>
</div>
<?php include 'footer.php'?>
