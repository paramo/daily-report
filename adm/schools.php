<?php
include 'header.php';

$resultMsg = enterNewSchool($conn);
?>
<?php menu_sidebar_admin(); ?>
    <div class="content">
        <div class="col-md-12">
            <h2>Schools</h2>
            <p>List existing schools and add new ones to the database. </p>
        </div>
        <div class="col-md-6 col-lg-6">
            <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="add_school_form">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <p for="schoolname" style="font-weight: bold">
                                Enter New School
                            </p>
                            <input type='text' name='schoolname' id='schoolname' />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            <input type="button" value="Add School" class="boton-sm ami btn-add-payment" onclick="schoolValidate(this.form, this.form.schoolname);" />
                        </div>
                    </div>
                    <p><?php echo $resultMsg; ?></p>
            </form>
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <!--<div class="panel-heading">Panel heading</div>-->

                <!-- Table -->
                <table class="table" id="simpleTable">
                    <thead>
                    <tr style="background-color: #313131; color: #fff ">
                        <th data-sort="string">
                            School ID <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            School Name <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php echo tableSchools($conn);?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php include 'footer.php' ?>