<?php
include 'header.php';
if(isset($_POST["ini_date"], $_POST["end_date"])){
    $iniDate = date_create_from_format('m/d/Y', $_POST["ini_date"])->format('Y-m-d');
    $endDate = date_create_from_format('m/d/Y', $_POST["end_date"])->format('Y-m-d');
} else {
    $iniDate = date("Y-m-d", strtotime("-1 months"));
    $endDate = date("Y-m-d");
}
?>
<?php menu_sidebar_admin(); ?>
    <div class="content">
        <div class="col-md-12">
            <h2>Members Report</h2>
            <p></p>
        </div>
        <div class="col-md-12 col-lg-12">
            <div class="row">
              <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="date_form">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <p for="from">From</p>
                    <input type="text" id="from" name="ini_date" value="<?php echo date_create_from_format('Y-m-d', $iniDate)->format('m/d/Y'); ?>">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <p for="to">To</p>
                    <input type="text" id="to" name="end_date" value="<?php echo date_create_from_format('Y-m-d', $endDate)->format('m/d/Y'); ?>">
                </div>
                  <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                      <input type="submit" class="boton-sm ami btn-add-payment" value="Go" />
                  </div>
              </form>
            </div>
            <div class="container-outer">
                <div class="container-inner">
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <!--<div class="panel-heading">Panel heading</div>-->
                        <!-- Table -->
                        <table class="table" id="membersTable">
                            <thead>
                                <tr style="background-color: #313131; color: #fff ">
                                    <th data-sort="string">
                                        School
                                    </th>
                                    <th data-sort="string">
                                        Appointments
                                    </th>
                                    <th data-sort="string">
                                        Show Up
                                    </th>
                                    <th data-sort="string">
                                        Show up Rate
                                    </th>
                                    <th data-sort="string">
                                        New Members
                                    </th>
                                    <th data-sort="string">
                                        Convertion Rate
                                    </th>
                                    <th data-sort="string">
                                        New Student Value
                                    </th>
                                    <th data-sort="string">
                                        Active Students
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php echo memberReport($conn, $iniDate, $endDate); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $( "#from" ).datepicker({
                defaultDate: "+1w",
                changeMonth: false,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                    $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
            });
            $( "#to" ).datepicker({
                defaultDate: "+1w",
                changeMonth: false,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                    $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    </script>
<?php include 'footer.php' ?>