<?php
include_once '../includes/register.inc.php';
include 'header.php';
?>
<?php menu_sidebar_admin(); ?>
<div class="content">
    <div class="col-md-12">
        <h2>Users</h2>
        <p>
        </p>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">New User</h3>
            </div>
            <div class="panel-body n-p-l-r">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php
                    if (!empty($error_msg)) {
                        echo $error_msg;
                    }?>
                </div>

                <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="registration_form">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p for="username" style="font-weight: bold">
                                    Username
                                </p>
                                <input type='text' name='username' id='username' />
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p for="email" style="font-weight: bold">
                                    Email
                                </p>
                                <input type="text" name="email" id="email" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p style="font-weight: bold">
                                    Password
                                </p>
                                <input type="password" name="password" id="password"/>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p style="font-weight: bold">
                                    Confirm Password
                                </p>
                                <input type="password" name="confirmpwd" id="confirmpwd" />
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p for="select-school" style="font-weight: bold">
                                    School
                                </p>
                                <?php echo listSchools($conn); ?>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <input type="button" value="Register" class="boton-sm ami btn-add-payment" onclick="return regformhash(this.form, this.form.username, this.form.email, this.form.password, this.form.confirmpwd);" />
                                <!--<a class="boton-sm ami btn-add-payment" href="#" role="button">Add</a>-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <?php if($_GET["err"] == "SUCCESS") {
                            echo "<p>The New User has been added to the Database.</p>";
                        }
                        elseif($_GET["err"] == "FAILURE") {
                            echo "<p>ERROR: Unable to Register New User.</p>";
                        }
                        ?>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <ul style="font-size: 0.8em;">
                            <li>Usernames may contain only digits, upper and lowercase letters and underscores</li>
                            <li>Emails must have a valid email format</li>
                            <li>Passwords must be at least 6 characters long</li>
                            <li>Passwords must contain
                                <ul>
                                    <li>At least one uppercase letter (A..Z)</li>
                                    <li>At least one lowercase letter (a..z)</li>
                                    <li>At least one number (0..9)</li>
                                </ul>
                            </li>
                            <li>Your password and confirmation must match exactly</li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <!--<div class="panel-heading">Panel heading</div>-->

            <!-- Table -->
            <table class="table" id="usrTable">
                <thead>
                    <tr style="background-color: #313131; color: #fff ">
                        <th data-sort="string">
                            Username <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="date">
                            Email <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                        <th data-sort="string">
                            School <i class="glyphicon glyphicon-chevron-up"></i> <i class="glyphicon glyphicon-chevron-down"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>
                <?php echo admListInstructor($conn); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php include 'footer.php'?>
