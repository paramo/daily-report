<?php
include 'header.php';
$mercMsg = addMerchandise($conn);
?>
<?php menu_sidebar_admin(); ?>
<div class="content">
    <div class="col-md-12">
        <h2>Merchandise</h2>
        <p>
        </p>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">New Merchandise</h3>
            </div>
            <div class="panel-body n-p-l-r">
              <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="member_form">
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p for="select-reason" style="font-weight: bold">
                        Customer
                    </p>
                    <input type="text" name="merCust" placeholder="" />
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Merchandise
                    </p>
                    <input type="text" name="merchandise" placeholder="Black uniform" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Description (140 characters)
                    </p>
                    <input type="text" name="merDesc" placeholder="PRIVATE LESSONS" />
                </div>
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Date
                    </p>
                    <input type="text" name="merDate" placeholder="10/24/2015" class="datepicker" id="datepicker-pay"/>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Payment Method
                    </p>
                    <select id="select-reason" name="merMethod">
                        <option value="cash">Cash</option>
                        <option value="check">Check</option>
                        <option value="asf">ASF</option>
                        <option value="cc">CC</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <p style="font-weight: bold">
                        Net Collected
                    </p>
                    <input type="text" name="merAmount" placeholder="1200" style="background-color: #F7E0E0; font-weight: bold" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <input type="button" class="boton-sm ami btn-add-payment" value="Add" onclick="merchandiseValidate(this.form, this.form.merCust, this.form.merchandise, this.form.merDate);" />
                    <p><?php echo $mercMsg; ?></p>
                </div>
              </form>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <!--<div class="panel-heading">Panel heading</div>-->

            <!-- Table -->
                    <?php echo listMerchandise($conn); ?>
        </div>
    </div>

</div>
<?php include 'footer.php'?>
