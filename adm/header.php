<?php
include_once '../includes/db_conn.php';
include_once '../includes/functions.php';

sec_session_start();

if (login_check($conn) == true && $_SESSION['user_role'] == 'administrator') :
//if (true):
    ?>
    <!doctype html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
    <!--[if gt IE 8]><!-->
    <html class="no-js" lang="">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Daily Report - Winners Martial Arts</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="../apple-touch-icon.png">
        <link rel="icon" href="../favicon.png">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <style>
            body {
                padding-top: 60px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" media="print" href="../css/print.css">
        <link rel="stylesheet" href="../css/jquery-ui.min.css">
        <script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="../js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <script src="../js/sha512.js" type="text/JavaScript"></script>
        <script src="../js/forms.js" type="text/JavaScript"></script>
    </head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="../dashboard.php">
            <img id="logo-in" src="../img/logo-in.svg">
        </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <form class="navbar-form navbar-right" role="form">
            <div class="form-group">
                <div class="left visible-sm-inline-block visible-md-inline-block visible-lg-inline-block visible-xs-block">
                    <p>
                        Welcome <?php echo htmlentities($_SESSION['username']); ?>
                    </p>
                    <a class="txt-sm" href="../includes/logout.php">
                        Logout
                    </a>
                </div>
                <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role'] == 'administrator'): ?>
                    <div class="admin-d">
                        <a class="boton-head ami btn-user-update" href="../dashboard.php" role="button">
                            Back
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </form>
    </div><!--/.navbar-collapse -->
</nav>
<?php else :
    header('Location: ../index.php');
endif; ?>