<?php
include 'header.php';
if(isset($_POST["ini_date"], $_POST["end_date"])){
    $iniDate = date_create_from_format('m/d/Y', $_POST["ini_date"])->format('Y-m-d');
    $endDate = date_create_from_format('m/d/Y', $_POST["end_date"])->format('Y-m-d');
} else {
    $iniDate = date("Y-m-d", strtotime("-1 months"));
    $endDate = date("Y-m-d");
}
$reportData = masterReportChart($conn, $iniDate, $endDate);
?>
<?php menu_sidebar_admin(); ?>
    <div class="content" style="width: 80%;">
        <div class="col-md-12">
            <h2>
                Chart
            </h2>
            <p>
                Master Report Chart
            </p>
        </div>
        <div class="col-md-12 col-lg-12 n-p-l-r">
            <div class="row">
                <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="date_form">
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <p for="from">From</p>
                        <input type="text" id="from" name="ini_date" value="<?php echo date_create_from_format('Y-m-d', $iniDate)->format('m/d/Y'); ?>">
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <p for="to">To</p>
                        <input type="text" id="to" name="end_date" value="<?php echo date_create_from_format('Y-m-d', $endDate)->format('m/d/Y'); ?>">
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <input type="submit" class="boton-sm ami btn-add-payment" value="Go" />
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <a href="javascript:window.print()" class="boton-sm-grey ami btn-add-payment"><i class="glyphicon glyphicon-print"></i> Print</a>
                    </div>
                </form>
            </div>
            <div class="col-md-12 col-lg-6 n-p-l-r">
            <div id="chart_div"></div>
            </div>
            <div class="col-md-12 col-lg-6 n-p-l-r">
            <div id="chart2_div"></div>
            </div>
        </div>
    </div>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {

            var data = google.visualization.arrayToDataTable([
                ['School', 'Billing', 'RNW/UPG/C.OUT', 'Private Lessons', 'Other', 'Merchandise', 'Testing', 'Pre-paid Testing'],
                <?php foreach($reportData as $data){
                                echo "['".$data["school_name"]."',".$data["billing"].",".$data["rnw_upg_cout"].",".$data["private"].",".$data["other"].",".$data["merchandise"].",".$data["testing"].",".$data["prepaid"]."],";
                } ?>
            ]);

            var options = {
                chartArea: {width: '65%'},
                hAxis: {minValue: 0, maxValue: 100000, gridlines: {count: 24}},
                vAxis: {},
                height: 700,
                bar: {groupWidth: "75%"},
                legend: { position: "none" },
                title: "Master Report Chart"
            };

            var data2 = google.visualization.arrayToDataTable([
                ['School', 'New Members'],
                <?php foreach($reportData as $data){
                                echo "['".$data["school_name"]."',".$data["new_member"]."],";
                } ?>
            ]);

            var options2 = {
                chartArea: {width: '65%'},
                hAxis: {minValue: 0, maxValue: 50, gridlines: {count: 12}},
                vAxis: {},
                height: 700,
                bar: {groupWidth: "75%"},
                legend: { position: "none" },
                title: "New Members"
            };

            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

            chart.draw(data, options);

            var chart2 = new google.visualization.BarChart(document.getElementById('chart2_div'));

            chart2.draw(data2, options2);
        }

        $(function() {
            $( "#from" ).datepicker({
                defaultDate: "+1w",
                changeMonth: false,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                    $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
            });
            $( "#to" ).datepicker({
                defaultDate: "+1w",
                changeMonth: false,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                    $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    </script>
<?php include 'footer.php' ?>