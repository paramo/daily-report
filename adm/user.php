<?php
include 'header.php';

$resultMsg = updateUserData($conn);

//GET INSTRUCTOR INFO
$userid = 0;
if(isset($_GET["usrid"])) {
    $userid = $_GET["usrid"];
}
elseif(isset($_POST["user_id"])) {
    $userid = $_POST["user_id"];
}
//$userid = $_GET["usrid"];
$query = "SELECT `user_id`, `username`, `email`, `school`, `password` FROM `Users` WHERE `user_id` = ". $userid;
$user = $conn->query($query);
$userData = $user->fetch_assoc();
?>
<?php menu_sidebar_admin(); ?>
<div class="content">
    <div class="col-md-12">
        <h2>User</h2>
        <p class="breadcrumb">
            <a href="users.php">Users</a> > <label><?php echo $userData["username"] ?></label>
        </p>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body n-p-l-r">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php
                    if (!empty($error_msg)) {
                        echo $error_msg;
                    }?>
                </div>

                <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="edit_user_form">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input type="hidden" name="user_id" value="<?php echo $userid; ?>" />
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p for="username" style="font-weight: bold">
                                    Username
                                </p>
                                <input type='text' name='username' id='username' value="<?php echo $userData['username']; ?>" />
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p for="email" style="font-weight: bold">
                                    Email
                                </p>
                                <input type="text" name="email" id="email" value="<?php echo $userData['email']; ?>" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p style="font-weight: bold">
                                    Password
                                </p>
                                <input type="password" name="password" id="password"/>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p style="font-weight: bold">
                                    Confirm Password
                                </p>
                                <input type="password" name="confirmpwd" id="confirmpwd" />
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <p for="select-school" style="font-weight: bold">
                                    School
                                </p>
                                <?php echo listSchools($conn); ?>
                            </div>
                            <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                <input type="button" value="Apply" class="boton-sm ami btn-add-payment" onclick="return regformhash(this.form, this.form.username, this.form.email, this.form.password, this.form.confirmpwd);" />
                                <!--<a class="boton-sm ami btn-add-payment" href="#" role="button">Add</a>-->
                            </div>
                            <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                <a class="boton-sm-grey ami btn-add-payment" href="#" role="button">Delete</a>
                            </div>
                        </div>
                        <p><?php echo $resultMsg; ?></p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <ul style="font-size: 0.8em;">
                            <li>Usernames may contain only digits, upper and lowercase letters and underscores</li>
                            <li>Emails must have a valid email format</li>
                            <li>Passwords must be at least 6 characters long</li>
                            <li>Passwords must contain
                                <ul>
                                    <li>At least one uppercase letter (A..Z)</li>
                                    <li>At least one lowercase letter (a..z)</li>
                                    <li>At least one number (0..9)</li>
                                </ul>
                            </li>
                            <li>Your password and confirmation must match exactly</li>
                            <li>NOTE: Filling the password fields is required to change user information.</li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$('.school_select option[value="<?php echo $userData["school"] ?>"]').attr('selected', 'selected');
</script>
<?php include 'footer.php'?>
