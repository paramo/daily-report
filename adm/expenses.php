<?php include 'header.php';?>
<?php
$expMsg = addExpense($conn);
$query = "SELECT `SchoolID`, `Name` FROM `School`";
$schools = $conn->query($query);

$schools_select = "<select name = 'expSchool'>";
while (($row = $schools->fetch_array()) != null)
{
    $schools_select .= "<option value = '{$row['SchoolID']}'";
    $schools_select .= ">{$row['Name']}</option>";
}
$schools_select .= "</select>";
?>
<?php menu_sidebar_admin(); ?>
    <div class="content">
        <div class="col-md-12">
            <h2>Expenses</h2>
            <p>List and add school expenses.</p>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Expense</h3>
                </div>
                <div class="panel-body n-p-l-r">
                    <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="expenses_form">
                        <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                            <label>
                                School
                            </label>
                            <?php echo $schools_select; ?>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                            <p for="select-reason" style="font-weight: bold">
                                Reason
                            </p>
                            <select id="select-reason" name="expReason">
                                <option value="" selected="selected" disabled="disabled">Select</option>
                                <option value="" disabled="disabled">-- Utilities --</option>
                                <option value="Rent">Rent</option>
                                <option value="Electricity">Electricity</option>
                                <option value="Water">Water</option>
                                <option value="Internet">Internet</option>
                                <option value="Phone">Phone</option>
                                <option value="Gas">Gas</option>
                                <option value="" disabled="disabled">-- ATA Fees / Insurance --</option>
                                <option value="ATA-Licence">ATA Licence</option>
                                <option value="ATA-Flat-Testing">ATA Flat Testing</option>
                                <option value="G-L-Insurance">G L Insurance</option>
                                <option value="Van-Insurance">Van Insurance</option>
                                <option value="Insurance-Bonds">Insurance Bonds</option>
                                <option value="Tournaments">Tournaments</option>
                                <option value="" disabled="disabled">-- Payroll --</option>
                                <option value="Payroll">Payroll</option>
                                <option value="Payroll-Taxes">Payroll Taxes</option>
                                <option value="Corporate-Payroll">Corporate Payroll</option>
                                <option value="Corporate-Taxes">Corporate Taxes</option>
                                <option value="Master-C-Salary">Master C Salary</option>
                                <option value="Yearly-Profit-Taxes">Yearly Profit Taxes</option>
                                <option value="Property-Taxes">Property-Taxes</option>
                                <option value="" disabled="disabled">-- Supplies --</option>
                                <option value="Cleaning-Supplies">Cleaning Supplies</option>
                                <option value="Office-Supplies">Office Supplies</option>
                                <option value="Maintenance-Repairs">Maintenance / Repairs</option>
                                <option value="" disabled="disabled">-- Vehicle Expenses --</option>
                                <option value="Van-Payments">Van Payments</option>
                                <option value="Van-Gas">Van Gas</option>
                                <option value="Van-Repairs">Van Repairs</option>
                                <option value="Van-Registration">Van Registration</option>
                                <option value="Other-Vehicle">Other Vehicle</option>
                                <option value="" disabled="disabled">-- Bank & Other Fees --</option>
                                <option value="Bank-Fee">Bank Fee</option>
                                <option value="Merchant-Fee">Merchant Fee</option>
                                <option value="Corporate-Fee">Corporate Fee</option>
                                <option value="" disabled="disabled">-- Miscellaneous --</option>
                                <option value="Miscellaneous">Miscellaneous</option>
                                <option value="Refunds">Refunds</option>
                                <option value="Merchandise">Merchandise</option>
                                <option value="Marketing">Marketing</option>
                                <option value="Continue-Education">Continue Education</option>
                                <option value="Loans">Loans</option>
                                <option value="Food">Food</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <p style="font-weight: bold">
                                Description (140 characters)
                            </p>
                            <input type="text" name="expDesc" placeholder="FLOOR REPAIR" />
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <p style="font-weight: bold">
                                Date
                            </p>
                            <input type="text" placeholder="10/24/2015" class="datepicker" id="datepicker-pay" name="expDate"/>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <p style="font-weight: bold">
                                Expense Amount
                            </p>
                            <input type="text" name="expAmount" placeholder="1200" style="background-color: #F7E0E0; font-weight: bold" />
                        </div>
                        <div class="col-md-8 col-lg-4 col-sm-12 col-xs-12">
                            <input type="submit" class="boton-sm ami btn-add-payment" value="Add" />
                            <p><?php echo $expMsg; ?></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <!--<div class="panel-heading">Panel heading</div>-->

                <!-- Table -->
                <?php echo listExpenses($conn); ?>
            </div>
        </div>
    </div>
<?php include 'footer.php' ?>