<?php include 'header.php';?>
<?php menu_sidebar(); ?>
<div class="content">
    <div class="col-md-12">
        <h2>New member</h2>
        <p>
        </p>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Member information</h3>
            </div>
            <div class="panel-body n-p">
              <form action="includes/add_new_member.php" method="post" name="new_member_form">
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        <label style="margin-top: 8px">
                            First Name
                        </label>
                        <input type="text" name="member_fname" placeholder="John"/>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <label  style="margin-top: 8px">
                            Last Name
                        </label>
                        <input type="text" name="member_lname" placeholder="Smith"/>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <label  style="margin-top: 8px">
                            Entry Date
                        </label>
                        <input type="text" name="member_date" placeholder="10/24/2015" class="datepicker" id="datepicker-update"/>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                        <label for="select-type" style="margin-top: 8px">
                            Type
                        </label>
                        <select id="select-type" name="member_type">
                            <option value="" selected="selected" disabled="disabled">Select</option>
                            <option value="Trial-Program">Trial Program</option>
                            <option value="Master-Club">Master Club</option>
                            <option value="Leadership">Leadership</option>
                            <option value="Legacy">Legacy</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <label  style="margin-top: 8px">
                            School
                        </label>
                        <?php echo listSchools($conn); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4 n-p-l-r">
                        <p><input type="button" class="boton-sm ami btn-user-update" value="Create" onclick="memberValidate(this.form, this.form.member_fname, this.form.member_lname, this.form.member_date);" /></p>
                        </div>
                        <div class="col-md-8">
                            <?php if($_GET["err"] == "SUCCESS") {
                                echo '<p>Member added successfully!</p>';
                            } elseif($_GET["err"] == "FAILURE") {
                                echo '<p>Member register ERROR</p>';
                            } ?>
                        </div>
                    </div>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'?>
